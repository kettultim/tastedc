<?php

$step = 'Set up payments';

drush_log(dt('Start: @step', array('@step' => $step)), 'ok');
ini_set('memory_limit', '1024M');

// Set up Secure Pages
variable_set('securepages_enable', 1);
variable_set('securepages_basepath', 'http://www.tastedc.com');
variable_set('securepages_basepath_ssl', 'https://www.tastedc.com');
// Add our checkout paths.
$securepages_pages = variable_get('securepages_pages', SECUREPAGES_PAGES);
$securepages_pages .= "\ncheckout/*\ncart\ndashboard/add-new-event";
variable_set('securepages_pages', $securepages_pages);

// Create our new product.
$product = new stdClass();
$product->sku = 'paid-event-2013';
$product->status = 1;
$product->uid = 1;
$product->type = 'commerce_node_checkout';
$product->title = 'Pay to post an event';
$product->commerce_price = array('und' => 
  array(
    0 => 
    array(
      'amount' => '1099',
      'currency_code' => 'USD',
      'data' => 
      array(
        'components' => 
          array(
        ),
      ),
    ),
  ),
);
commerce_product_save($product);

// Assign the new product to our event content type.
variable_set('commerce_node_checkout_products_event_listing', array(1));

drupal_flush_all_caches();
drush_log(dt('Done: @step', array('@step' => $step)), 'ok');
