#!/bin/bash

# Used for file paths
RELEASE_TAG=`basename $0`
RELEASE_TAG="${RELEASE_TAG%.*}"

# Show when we started
RELEASE_START=$(date +%s)

# Support janky settings.php db connection logic.
URI='http://social-driver.com/tastedc'

# Welcome folks to Drupal dependency hell. Drupal Commerce can't be unenabled
# and re-enabled as all of it's logic to create fields, etc, is in
# hook_enable().
# @see http://drupal.org/node/1361562
# @see http://drupal.org/node/858722
# @see http://drupal.org/node/1280528
drush --uri="$URI" dis event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y
drush --uri="$URI" pm-uninstall event_content_type,commerce,commerce_add_to_cart_extras,commerce_braintree,commerce_cardonfile,commerce_cart,commerce_checkout,commerce_credits,commerce_credits_ui,commerce_customer,commerce_customer_profile_dummy_type,commerce_customer_ui,commerce_features,commerce_funds,commerce_funds_payment,commerce_invoice_receipt,commerce_line_item,commerce_line_item_ui,commerce_node_checkout,commerce_node_checkout_expire,commerce_order,commerce_order_ui,commerce_payment,commerce_payment_dummy_offsite,commerce_payment_example,commerce_payment_ui,commerce_price,commerce_product,commerce_product_crud_test,commerce_product_pricing,commerce_product_pricing_ui,commerce_product_reference,commerce_product_ui,commerce_tax,commerce_tax_ui,commerce_ui, tdc_commerce -y

# We had to disable our event content type features module to disable Commerce.
# Disabling and re-enabling it does not cause any data loss, so this is "safe"
# in a release script.
drush --uri="$URI" en event_content_type -y

# Turn on new modules, slowly, so Commerce doesn't throw a hissy.
drush --uri="$URI" en emogrifier strongarm -y
drush --uri="$URI" en commerce_line_item commerce_product_ui -y
drush --uri="$URI" en commerce_customer_ui commerce_ui commerce_order commerce_product_reference -y
drush --uri="$URI" en commerce_braintree commerce_features commerce_cart commerce_checkout commerce_product_pricing_ui commerce_invoice_receipt commerce_node_checkout securepages commerce_price, commerce_product, commerce_add_to_cart_extras, commerce_customer, commerce_line_item, commerce_payment, commerce_order_ui, commerce_line_item_ui, commerce_product_ui, commerce_payment_ui -y
drush --uri="$URI" en tdc_commerce -y

# Ensure all settings tracked in Features get set.
drush --uri="$URI" features-revert tdc_commerce -y

# Set up payment integration.
drush --uri="$URI" php-script 'case-154-payments.php' --script-path="releases/${RELEASE_TAG}" --user=tastedc

echo 'Step needed! Go to (admin/commerce/config/payment-methods) disable any payment gateways enabled, enable Braintree, and set the API keys.'

# Time we ended
RELEASE_END=$(date +%s)
RELEASE_DURATION=$(( ($RELEASE_END - $RELEASE_START)/60 ))
echo "Deployment script completed! It took $RELEASE_DURATION minutes."
