<?php


?>

<div class="fluid">
            
  <div class="widget grid12">   

    <div class="whead"><h6><?php print $form['#node']->nid ? t('Edit Event') : t('New Event'); ?></h6><div class="clear"></div></div>

    <div class="body">

      <p>We're here to help you pack out your events. For best results... We're here to help you pack out your events. For best results...
      We're here to help you pack out your events. For best results... We're here to help you pack out your events. For best results... We're here to help you pack out your events. For best results...
      We're here to help you pack out your events. For best results... We're here to help you pack out your events. For best results...</p>

      <div class="divider"><span></span></div>

      <?php print render($form['title']); ?>
      <?php print render($form['field_short_description']); ?>      
      <?php print render($form['field_phone']); ?>

      <div class="widget">
        <div class="whead"><h6><?php print t('Date / Time'); ?></h6><div class="clear"></div></div>
        <div class="body event-date-line">
          <?php print render($form['field_date_time']); ?>
        </div>
      </div>
      
      <div class="widget">
        <div class="whead"><h6><?php print t('Location'); ?></h6><div class="clear"></div></div>
        <div class="body">

          <div class="form-item form-type-radio"><input type="radio" name="locationtype" id="locationtype0" value="0"> <label class="option" for="locationtype0"><?php print t("Select a venue I've used before"); ?></label></div>

          <?php print render($form['field_previous_location']); ?>

          <div class="form-item form-type-radio"><input type="radio" name="locationtype" id="locationtype1" value="1"> <label class="option" for="locationtype1"><?php print t('New location'); ?></label></div>

          <?php print render($form['field_location']); ?>

        </div>
      </div>
     
      <?php print render($form['field_location_website']); ?>
      <?php print render($form['field_event_type']); ?>
      <div class="categiries-checkboxes">
      <?php print render($form['field_category']); ?>
      </div>      
      <?php print render($form['field_image']); ?>

      <div class="widget">
        <div class="whead"><h6><?php print t('Feature Your Event on TasteDC?'); ?></h6><div class="clear"></div></div>
        <div class="body">

          <?php print render($form['field_promote_event']); ?>

          <div class="promote-event-text">
            <p>TasteDC has a unique and powerful Marketing Platform to drive traffic and ultimately fill up your food and drink events. 
            For a small fee, you can FEATURE your event and GUARANTEE it’s ultimate success. 
            Note: Featured Events get over THREE TIMES the traffic of our Free Listings and you can measure the RESULTS!</p>
            <br>

            FEATURED EVENTS include:<br />
            <ul style="margin-left: 10px;">
              <li>&bull; a FANTASTIC SITE on TasteDC - all the Bells and Whistles including large graphics, lots of details on the event and a better position on the TasteDC website..</li>
              <li>&bull; Social Media: We promote your event to our over 6,000 Twitter and Facebook Followers and more..We let everyone know about your Facebook, Twitter and other Social Media. Why just sell your event - get the added BENEFIT of creating NEW CUSTOMERS!</li>
              <li>&bull; Your own EVENT CAMPAIGN REPORT (You Get an Event Dashboard..) - we measure how many people saw your Event, how many people Discovered your business, and More..we can even measure your ticket sales coming from our EXCITING Event Platform</li>
            </ul>

            <br>
            Added Benefit of Featured Events..<br>
            <br>
            Featured Events get the BEST POSITION on our EMAIL -&nbsp; the TasteDC Food and Drink Event Calendar which currently is delivered to OVER 10,000 SUBSCRIBERS IN THE Washington, D.C. area..Email DRIVES TRAFFIC to your site..it’s Amazing!
          </div>

          <div id="paid-event-form-elements">
	          <?php print render($form['field_buy_ticket_link']); ?>
            <?php print render($form['field_tickets_available']); ?>
	          <?php print render($form['field_sale_end_date']); ?>
	          <?php print render($form['field_price']); ?>
	          <?php print render($form['field_subtitle']); ?>
	          <?php print render($form['body']); ?>
	          <?php print render($form['field_image_2']); ?>
	      </div>

        </div>
      </div>

      <?php print drupal_render_children($form); ?>
    </div>
      
    <div class="clear"></div>    

  </div>

</div>