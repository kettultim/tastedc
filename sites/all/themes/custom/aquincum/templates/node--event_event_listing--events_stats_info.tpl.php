<?php

 hide($content['field_image']);
 hide($content['field_image_2']);
 hide($content['field_date_time']);
 hide($content['field_location']);
 hide($content['field_category']);
 hide($content['field_location_website']);
 hide($content['field_buy_ticket_link']);
 hide($content['field_short_description']);
 hide($content['field_event_type']);
 hide($content['field_tickets_available']);

?>
<div class="fluid event-details-block">
            
    <div class="grid6">
        <div class="body">

          <?php if ( function_exists('_tdc_helper_event_date') && isset($node->field_date_time[LANGUAGE_NONE]) ) : ?>
          <div class="field field-name-field-date field-type-datetime field-label-above">
            <div class="field-label"><?php print t('Date:'); ?>&nbsp;</div>
            <div class="field-items">
              <div class="field-item even">
                <span>
                  <?php print _tdc_helper_event_date($node, 'date'); ?>
                </span>
              </div>
            </div>
          </div>
          <div class="field field-name-field-time field-type-datetime field-label-above">
            <div class="field-label"><?php print t('Time:'); ?>&nbsp;</div>
            <div class="field-items">
              <div class="field-item even">
                <span>
                  <?php print _tdc_helper_event_date($node, 'time'); ?>
                </span>
              </div>
            </div>
          </div>
          <?php else : ?>
            <?php print render($content['field_date_time']); ?>
          <?php endif; ?>


          <?php print render($content['field_location']); ?>

          <?php print render($content['body']); ?>

          <?php print render($content); ?>

        </div>
    </div>
    
        
    <div class="grid6 right-column">
        <div class="body">

          <?php print render($content['field_event_type']); ?>

          <?php if ( function_exists('_tdc_helper_event_categories') && isset($node->field_category[LANGUAGE_NONE])) : ?>

            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('Tags:'); ?>&nbsp;</div>
              <div class="field-items">
                <?php print _tdc_helper_event_categories($node); ?>
              </div>
            </div>

          <?php else : ?>
            <?php print render($content['field_category']); ?>
          <?php endif; ?>

          <?php if ( user_is_logged_in() ) $profile2 = profile2_load_by_user($user); ?>

          <?php if ( isset($profile2['main']) && isset($profile2['main']->field_twitter[LANGUAGE_NONE])) : ?>
            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('Twitter:'); ?>&nbsp;</div>
              <div class="field-items">
                <a target="_blank" href="<?php print $profile2['main']->field_twitter[LANGUAGE_NONE][0]['value']; ?>"><?php print $profile2['main']->field_twitter[LANGUAGE_NONE][0]['value']; ?></a>
              </div>
            </div>
          <?php endif; ?>

          <?php if ( isset($profile2['main']) && isset($profile2['main']->field_facebook[LANGUAGE_NONE])) : ?>
            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('Facebook:'); ?>&nbsp;</div>
              <div class="field-items">
                <a target="_blank" href="<?php print $profile2['main']->field_facebook[LANGUAGE_NONE][0]['value']; ?>"><?php print $profile2['main']->field_facebook[LANGUAGE_NONE][0]['value']; ?></a>
              </div>
            </div>
          <?php endif; ?>

          <?php if ( isset($profile2['main']) && isset($profile2['main']->field_yelp[LANGUAGE_NONE])) : ?>
            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('Yelp:'); ?>&nbsp;</div>
              <div class="field-items">
                <a target="_blank" href="<?php print $profile2['main']->field_yelp[LANGUAGE_NONE][0]['value']; ?>"><?php print $profile2['main']->field_yelp[LANGUAGE_NONE][0]['value']; ?></a>
              </div>
            </div>
          <?php endif; ?>

          <?php if ( isset($profile2['main']) && isset($profile2['main']->field_zagat[LANGUAGE_NONE])) : ?>
            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('Zagat:'); ?>&nbsp;</div>
              <div class="field-items">
                <a target="_blank" href="<?php print $profile2['main']->field_zagat[LANGUAGE_NONE][0]['value']; ?>"><?php print $profile2['main']->field_zagat[LANGUAGE_NONE][0]['value']; ?></a>
              </div>
            </div>
          <?php endif; ?>

          <?php if ( isset($profile2['main']) && isset($profile2['main']->field_opentable[LANGUAGE_NONE])) : ?>
            <div class="field field-name-field-category field-type-taxonomy-term-reference field-label-inline clearfix">
              <div class="field-label"><?php print t('OpenTable:'); ?>&nbsp;</div>
              <div class="field-items">
                <a target="_blank" href="<?php print $profile2['main']->field_opentable[LANGUAGE_NONE][0]['value']; ?>"><?php print $profile2['main']->field_opentable[LANGUAGE_NONE][0]['value']; ?></a>
              </div>
            </div>
          <?php endif; ?>

         <div class="photos-label"><strong><?php print t('Photos:'); ?></strong></div>
         <div class="event-images">
           <?php print render($content['field_image']); ?>
           <?php print render($content['field_image_2']); ?>
         </div>

        </div>
    </div>

    <div class="clear"></div>
    
</div>