<div id="page-wrapper">

  <div id="top">
    <div class="wrapper section clearfix">

      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="logo">
          <img src="<?php print base_path() . drupal_get_path('theme', 'aquincum') . '/images/logo_small.png' ?>" />
        </a>
      <?php endif; ?>

    <?php if ( user_is_logged_in() ) : ?>
    <div class="topNav">
        <ul class="userNav">
            <li><?php print l('', '<front>', array('attributes' => array('title' => t('Home'), 'class' => array('home')))) ?></li>
            <li><?php print l('', 'user/logout', array('attributes' => array('title' => t('Logout'), 'class' => array('logout')))) ?></li>
        </ul>
    </div>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
    <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

      <?php if ($site_name): ?>
      <?php if ($title): ?>
      <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
        <strong>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
        </strong>
      </div>
    <?php else: /* Use h1 when the content title is empty */ ?>
    <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
    </h1>
  <?php endif; ?>
<?php endif; ?>

<?php if ($site_slogan): ?>
  <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
    <?php print $site_slogan; ?>
  </div>
<?php endif; ?>

</div> <!-- /#name-and-slogan -->
<?php endif; ?>

<?php print render($page['header']); ?>     

</div>
</div> <!-- /.section, /#header -->

  <?php if ($page['sidebar']): ?>
  <div id="sidebar" class="column sidebar <?php if (!empty($page['secondary_sidebar'])) print 'with-secondary-sidebar' ?>" >
    <div class="mainNav">
      <?php print render($page['sidebar']); ?>
    </div>  
  </div> <!-- /.section, /#sidebar -->
<?php endif; ?>

<div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

<?php if (!empty($page['content_top'])): ?>
  <?php print render($page['content_top']); ?>
  <div class="clear"></div>
<?php endif ?>

<?php if ($page['secondary_sidebar']): ?>
  <div id="secondary_sidebar">
    <div class="secNav">
      <div class="secWrapper">
        <div class="divider"><span></span></div>
        <?php print render($page['secondary_sidebar']); ?>
      </div>       
    </div>
  </div> <!-- /.section, /#sidebar -->
<?php endif; ?>

<div id="content" class="<?php if (!empty($page['secondary_sidebar'])) print 'with-secondary-sidebar' ?>" >     
  <?php if ($tabs): ?>
  <div class="etabs">
    <?php print render($tabs); ?>
  </div>
<?php endif; ?>

<?php print render($page['help']); ?>

<?php if ($messages): ?>
  <div class="wrapper"><div id="messages">
    <?php print $messages; ?>
  </div></div> <!-- /.section, /#messages -->
<?php endif; ?>

<?php if ($action_links): ?>
  <ul class="action-links">
    <?php print render($action_links); ?>
  </ul>
<?php endif; ?>

<?php print render($page['content']); ?>
</div>

</div></div> <!-- /#main, /#main-wrapper -->

</div> <!-- /#page, /#page-wrapper -->
