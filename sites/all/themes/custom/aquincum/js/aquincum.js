/**
 * @file
 * Aquincum Theme js helper.
 */

(function ($) {
  Drupal.behaviors.aquincumThemeHelper = {
    attach: function(context) {

      var conetntHeight = $('#content .region-content .content').height();
      var heightOffset = 300;
      $('#sidebar').height(conetntHeight + heightOffset);
      $('.page-node-add #sidebar').height(conetntHeight + heightOffset + 150);

    }
  };
}(jQuery));