<?php

/**
 * Implements hook_theme().
 */
function aquincum_theme($existing, $type, $theme, $path) {

  return array(
    'event_listing_node_form' => array(
      'render element' => 'form',
      'template' => 'listing-node-form',
      'path' => drupal_get_path('theme', 'aquincum') . '/templates',
    ),
  );

}

function aquincum_preprocess_page(&$variables) {
  $variables['primary_local_tasks'] = menu_primary_local_tasks();
  $variables['secondary_local_tasks'] = menu_secondary_local_tasks();  

  $is_edit_mode = (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit');

  if ($is_edit_mode) {
    unset($variables['tabs']);
  }

  foreach ($variables['page']['sidebar']['menu_menu-dashboard-main'] as $key => $element) {    
    if (is_array($element) && $element['#href'] == 'dashboard') {      
      if ((arg(0) == 'dashboard' || current_path() == 'node/add/event-listing' || $is_edit_mode) 
        && !in_array('active-trail', $element['#localized_options']['attributes']['class'])) {
        $variables['page']['sidebar']['menu_menu-dashboard-main'][$key]['#localized_options']['attributes']['class'][] = 'active-trail';
      }
    }
  }
}

function aquincum_process_page(&$variables) {

}

function aquincum_menu_link__menu_dashboard_main(array $variables) {

  global $user;

  $element = $variables['element'];
  $theme_path = base_path() . drupal_get_path('theme', 'aquincum');

  $icon_path = ''; $icon = '';

  if (isset($element['#localized_options']['attributes']['class']) && in_array('active', $element['#localized_options']['attributes']['class']) || in_array('active-trail', $element['#localized_options']['attributes']['class'])) {

    $element['#localized_options']['attributes']['class'][] = 'clicked';
  }

  if (in_array('active', $element['#attributes']['class']) || 
      in_array('active-trail', $element['#attributes']['class'])) {

    $element['#attributes']['class'][] = 'clicked';
  }  

  switch ($element['#href']) {
    case 'dashboard':
      $icon_path = $theme_path . '/images/icons/mainnav/dashboard.png';
      break;
    case 'user':
      $icon_path = $theme_path . '/images/icons/mainnav/user.png';
      $element['#href'] = 'user/' . $user->uid;
      break;
    case 'dashboard/events':
      $icon = '<span class="icos-mcalendar"></span> ';
      break;
    case 'dashboard/events/past':
      $icon = '<span class="icos-dcalendar"></span> ';
      break;      
  }

  if (!empty($icon_path)) {
    $icon = theme('image', array('path'=> $icon_path));
  }

  $content = $element['#title'];

  if (!empty($icon)) {
    $element['#localized_options']['html'] = TRUE;
    $content = $icon . '<span class="title">' . $element['#title'] . '</span>';
  }

  $output = l($content, $element['#href'], $element['#localized_options']);

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . "</li>\n";
}

function aquincum_menu_tree__menu_dashboard_main($variables) {
  return '<ul class="nav">' . $variables['tree'] . '</ul>';
}

function aquincum_menu_tree__menu_dashboard_main_events($variables) {  
  
  return '<ul class="iconsLine ic2 etabs">' . $variables['tree'] . '</ul>';  
}

function aquincum_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {

    $output .= '<ul id="breadcrumbs" class="breadcrumbs">' . implode('', $breadcrumb) . '</ul>';
    return $output;
  }
}

/**
 * Implements theme_menu_local_tasks().
 */
function aquincum_menu_local_tasks(&$variables) {
  $output = '';

  if ( !empty($variables['primary']) ) {

    $i = 0;
    foreach ( $variables['primary'] as $item => $link ) {

      $variables['primary'][$i]['#link']['localized_options']['class'] = array('primary-link-item');
      $i++;

    }

    $variables['primary']['#prefix'] = '<div class="primary-tabs">';
    $variables['primary']['#suffix'] = '</div>';
    $output .= drupal_render($variables['primary']);
  }

  if ( !empty($variables['secondary']) ) {
    $variables['secondary']['#prefix'] = '<div class="secondary-tabs">';
    $variables['secondary']['#suffix'] = '</div>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements theme_menu_local_task().
 */
function aquincum_menu_local_task($variables) {

  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }
  if ( in_array('primary-link-item', $link['localized_options']['class']) ) {
    return '<a href="' . url($link['href']) . '" ' . (!empty($variables['element']['#active']) ? ' class="buttonS bDefault active"' : ' class="buttonS bDefault"') . '>' . $link_text . "</a>\n";
  }
  else {
    return '<li' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>' . l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
  }
}

/*
 * Implements theme_status_messages().
 */
function aquincum_status_messages($variables) {

  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );

  foreach (drupal_get_messages($display) as $type => $messages) {

    switch ($type) {
      case "status":
        $alerttype = "nSuccess";
        break;
      case "error":
        $alerttype = "nFailure";
        break;
      case "warning":
      default;
        $alerttype = "nWarning";
        break;
    }

    $output = '<div class="nNote ' . $alerttype . '">';

    if ( count($messages) > 1 ) {
      //$output .= " <ul>";
      foreach ($messages as $message) {
       // $output .= '  <li>' . $message . "</li>";
       $output .= '  <p>' . $message . "</p>";
      }
      //$output .= " </ul>";
    }
    else {
      $output .= '<p>' . $messages[0] . '</p>';
    }

    $output .= "</div>";

  }

  return $output;

}

/**
 * Implements theme_button().
 */
function aquincum_button($variables) {

  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  $element['#attributes']['class'][] = 'sideB';
  $element['#attributes']['class'][] = 'bLightBlue';
  $element['#attributes']['class'][] = 'buttonL';

  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
    $element['#attributes']['class'][] = 'disabled';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';

}

/**
 * Implements theme_textfield().
 */
function aquincum_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}
