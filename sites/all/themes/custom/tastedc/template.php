<?php

/**
 * Add body classes if certain regions have content.
 */
function tastedc_preprocess_html(&$variables) {

  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  if ( user_access('access administration menu') ) {
    $variables['classes_array'][] = 'toolbar';
  }

  if ( arg(0) == 'node' && is_numeric(arg(1)) ) {

    $node = node_load(arg(1));

    if ( $node->type == 'event_listing' ) {
      if ( $node->field_promote_event[LANGUAGE_NONE][0]['value'] == 0 ) {
        $variables['classes_array'][] = 'node-type-paid-event-listing';
      }
      else {
        $variables['classes_array'][] = 'node-type-free-event-listing';
      }
    }

  }

  drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/jquery-ui-1.8.24.custom.min.js');

  drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/ga-events.js');
  drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/main.js');
  //drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/admin.js');
  //drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/google-map-api.js');
  drupal_add_js('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
  drupal_add_js(drupal_get_path('theme', 'tastedc') .'/scripts/jquery.ui.map.js');

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE', '!IE' => FALSE), 'preprocess' => FALSE));

}

/**
 * Override or insert variables into the page template for HTML output.
 */
function tastedc_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function tastedc_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
  
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function tastedc_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'tastedc') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function tastedc_process_maintenance_page(&$variables) {

  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }

}

/**
 * Override or insert variables into the node template.
 */
function tastedc_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }

  if ( isset($variables['node']) ) {
  
    $node = $variables['node'];

    if ($node->nid == 358) {
      //dsm($variables['content']);
    }

    if (arg(0) == 'events' && isset($variables['content']['field_location']['#items'][0])) {
      $location = $variables['content']['field_location']['#items'][0];
      $city = trim($location['locality']);
      $state = trim($location['administrative_area']);

      $location = $city;
      if (!empty($location) && !empty($state)) $location .= ', ';
      $location .= $state;
      if (!empty($location)) $location = '&nbsp;in ' . $location;

      unset($variables['content']['field_location']);
      $variables['content']['field_location'] = array(
        '#markup' => '<div class="field field-event-location"><div class="field-item">' . $location . '</div></div>',
        '#weight' => 2,
      );
    }    

    if ( $node->type == 'event_listing' ) {
      if ( $node->field_promote_event[LANGUAGE_NONE][0]['value'] == 0 ) {
        $variables['classes_array'][] = 'node-paid-event-listing';
      }
      else {
        $variables['classes_array'][] = 'node-free-event-listing';
        unset($variables['content']['body']);
        unset($variables['content']['field_buy_ticket_link']);
      }
    }

  }

}

/**
 * Override or insert variables into the block template.
 */
function tastedc_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function tastedc_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function tastedc_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}

/**
* Add / modify variables before the page renders.
*/
function tastedc_preprocess_page(&$variables) {

  global $user;
  
  if (in_array('vendor', $user->roles)) {
    foreach ($variables['secondary_menu'] as $key => $menu_item) {
        if ($menu_item['href'] == 'user') {
          $variables['secondary_menu'][$key]['href'] = 'dashboard';
        }
      }  
  }
  

  if ( isset($variables['node']) ) {

    $node = $variables['node'];

    if ( $node->type == 'event_listing' ) {
      if ( $node->field_promote_event[LANGUAGE_NONE][0]['value'] == 0 ) {
        $variables['classes_array'][] = 'node-type-paid-event-listing';
        $variables['theme_hook_suggestions'][] = 'page__paid_event_listing';
      }
      else {
        $variables['classes_array'][] = 'node-type-free-event-listing';
        $variables['theme_hook_suggestions'][] = 'page__free_event_listing';
      }

      if (isset($node->field_price['und'][0]['amount'])) {
        $variables['ticket_price'] = '<h3>Cost: $' . 
          number_format($node->field_price['und'][0]['amount'] / 100, 2, '.', '') . ' / ticket</h3>';
      }

      if (isset($node->field_buy_ticket_link['und'][0]['value'])) {
        $variables['buy_ticket_link'] = l(t('PURCHASE TICKETS'), $node->field_buy_ticket_link['und'][0]['value'],
          array('external' => TRUE, 'attributes' => array('class' => array('purchase-tickets'))));
      }

      $back_link_text = 'View Other Events';
      $front_url = url($path, array('absolute' => TRUE));
      $front_url = str_replace(array('http://www.', 'https://www.', 'http://', 'https://'), '', $front_url);      

      $referrer = $_SERVER['HTTP_REFERER'];
      $referrer = str_replace(array('http://www.', 'https://www.', 'http://', 'https://'), '', $referrer);      
      
      if ($front_url === $referrer) {
        $back_link_text = 'Back to Search Results';
      }
      
      $variables['back_link'] = l($back_link_text, '<front>', array('attributes' => array('id'=>'back')));      
    }

    $variables['theme_hook_suggestions'][] = 'page__' . $node->type;
  }

}

function getSocialUrl($link, $name, $imageSrc, $type = '') {
	global $base_url, $base_path;

  if (!empty($type)) $type = ' ' . $type;

	return "<div class=\"social-links$type\"><a href=\"$link\"><img src=\"" . $base_path . drupal_get_path('theme', 'tastedc') . "/images/social/$imageSrc\"></a><a href=\"$link\" class=\"social-text\"> $name</a></div>";
}

function format_phone($phone) {
  $phone = preg_replace("/[^0-9]/", "", $phone);
   
  if(strlen($phone) == 7)
    return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
  elseif(strlen($phone) == 10)
    return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
  else
    return $phone;
}

/**
* Register custom templates
*/
function tastedc_theme() {
  $items = array();

  $items['user_register_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'tastedc') . '/templates',
    'template' => 'user-register-form',
  );
 
  return $items;  
}

