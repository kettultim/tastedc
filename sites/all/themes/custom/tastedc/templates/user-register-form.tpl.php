<p>TasteDC is the most powerful tool on the web to promote your food and drink events.
We'll show you why people come to us first for their foodie educational and entertaiment needs!</p>

<p><strong>First-time Registration</strong><br>
If this is your first time posting an event OR you do not have a Log-In Username and Passcode, please fill in the following.
We want to make sure that your tasting events sell out and bring you many new customers.
The information below will help us better serve you - and if you would like to get MAXIMUM exposure for your events,
You can Feature Your Event, click <?php echo l('here', 'content/featured-events') ?> for more details.
</p>

<div class="tastedc-user-register-form-wrapper">
  <!-- Account Settings -->
  <fieldset class="form-wrapper">
    <legend><span class="fieldset-legend">Account Settings</span></legend>
    <div class="fieldset-wrapper">
      <?php echo render($form['account']) ?>
    </div>
  </fieldset>
  
  <!-- Profile -->
  <fieldset class="form-wrapper">
    <legend><span class="fieldset-legend">Profile</span></legend>
    <div class="fieldset-wrapper">
      <?php echo render($form['profile_main']['field_company_name']) ?>
      <?php echo render($form['profile_main']['field_company_website']) ?>
      
      
      <div style="margin-top: 40px;">
        <div style="width: 50%;float: left;">
        <strong>Event Contact 1</strong>
        <?php echo render($form['profile_main']['field_contact_1_first_name']) ?>
        <?php echo render($form['profile_main']['field_contact_1']) ?>
        <?php echo render($form['profile_main']['field_contact_1_title']) ?>
        <?php echo render($form['profile_main']['field_phone_1']) ?>        
        </div>
        <div style="width: 50%; float: left;">
        <strong>Event Contact 2</strong>
        <?php echo render($form['profile_main']['field_contact_2_first_name']) ?>
        <?php echo render($form['profile_main']['field_contact_2']) ?>
        <?php echo render($form['profile_main']['field_contact_2_title']) ?>
        <?php echo render($form['profile_main']['field_phone_2']) ?>        
        </div>        
      </div>
      <div class="clearfix"></div>
      
      <?php
      unset($form['profile_main']['field_company_address'][LANGUAGE_NONE][0]['street_block']['premise']);
      $form['profile_main']['field_company_address'][LANGUAGE_NONE][0]['street_block']['thoroughfare']['#title'] = t('Street Address');
      $form['profile_main']['field_company_address'][LANGUAGE_NONE][0]['street_block']['thoroughfare']['#size'] = 70;
      ?>
      <?php echo render($form['profile_main']['field_company_address']) ?>

      <fieldset class="form-wrapper">
        <legend><span class="fieldset-legend">Social Media</span></legend>
        <div class="fieldset-wrapper">
          <div class="fieldset-description">This is Optional Information. Social Media links will only be shown on Featured Events</div>      
          <?php echo render($form['profile_main']['field_facebook']) ?>
          <?php echo render($form['profile_main']['field_twitter']) ?>
          <?php echo render($form['profile_main']['field_company_blog']) ?>
          <?php echo render($form['profile_main']['field_yelp']) ?>
          <?php echo render($form['profile_main']['field_opentable']) ?>
          <?php echo render($form['profile_main']['field_zagat']) ?>
          <?php echo render($form['profile_main']['field_city_eats']) ?>
          <?php echo render($form['profile_main']['field_reviews']) ?>
        </div>
      </fieldset>      
      
      <?php echo render($form['profile_main']['field_business_type']) ?>
      <?php echo render($form['profile_main']['field_business_other']) ?>
      <?php echo render($form['profile_main']['field_event_category']) ?>
      
      <?php unset($form['profile_main']['field_venue'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_venue']) ?>
      <?php unset($form['profile_main']['field_venue_arrange_events'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_venue_arrange_events']) ?>
      <?php unset($form['profile_main']['field_private_event_space'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_private_event_space']) ?>
      <div class="form-item"><label>Event space capacity?</label></div>
      <div class="space_capacity">
      <?php echo render($form['profile_main']['field_space_capacity_standing']) ?>
      <?php echo render($form['profile_main']['field_space_capacity_seated']) ?>
      </div>
      <div class="clearfix"></div>      
      
      <?php unset($form['profile_main']['field_deal_sites'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_deal_sites']) ?>
      <?php unset($form['profile_main']['field_deal_sites_experience'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_deal_sites_experience']) ?>      
      
      <?php unset($form['profile_main']['field_number_of_events'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_number_of_events']) ?>
      
      <div class="form-item"><label>Do you have a Special Events Person/Manager who is responsible for booking and running these events?</label></div>
      <div class="event-manager">
      <?php echo render($form['profile_main']['field_event_manager_first_name']) ?>
      <?php echo render($form['profile_main']['field_event_manager_last_name']) ?>
      <?php echo render($form['profile_main']['field_event_manager_email']) ?>
      <?php echo render($form['profile_main']['field_event_manager_phone']) ?>
      </div>
      
      <div class="form-item"><label>Do you have a PR/publicity department and/or do you use an outside person/agency?</label></div>
      <div class="pr-department">
      <?php echo render($form['profile_main']['field_pr_department_first_name']) ?>
      <?php echo render($form['profile_main']['field_pr_department_last_name']) ?>
      <?php echo render($form['profile_main']['field_pr_department_email']) ?>
      <?php echo render($form['profile_main']['field_pr_department_phone']) ?>
      <?php unset($form['profile_main']['field_pr_department_location'][LANGUAGE_NONE]['_none']); ?>
      <?php echo render($form['profile_main']['field_pr_department_location']) ?>
      </div>
      
      <?php 
      unset($form['profile_main']['field_maximum_exposure'][LANGUAGE_NONE]['_none']); 
      $form['profile_main']['field_maximum_exposure'][LANGUAGE_NONE][1]['#title'] = 'Yes, we\'ll contact you with outstanding options that can deliver your desired target audience - people who dine out frequently and purchase wine/craft beer/drinks with their meals.';
      ?>
      <?php echo render($form['profile_main']['field_maximum_exposure']) ?>
    </div>
  </fieldset>  
  
  <?php unset($form['profile_main']) ?>
  <?php print drupal_render_children($form) ?>
</div>