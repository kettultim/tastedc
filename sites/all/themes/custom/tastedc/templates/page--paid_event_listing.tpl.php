<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<div id="page-wrapper"><div id="page">

  <div id="header" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>"><div class="section clearfix">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>

    <?php print render($page['header']); ?>

    <?php if ($main_menu): ?>
      <div id="main-menu" class="navigation">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>

    <?php if ($secondary_menu): ?>
      <div id="secondary-menu" class="navigation">
        <?php print theme('links__system_secondary_menu', array(
          'links' => $secondary_menu,
          'attributes' => array(
            'id' => 'secondary-menu-links',
            'class' => array('links', 'inline', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#secondary-menu -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#header -->

  <?php //if ($messages): ?>
    <!--<div id="messages"><div class="section clearfix">
      <?php //print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php //endif; ?>

  <?php if ($page['featured']): ?>
    <div id="featured"><div class="section clearfix">
      <?php print render($page['featured']); ?>
    </div></div> <!-- /.section, /#featured -->
  <?php endif; ?>

  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <div id="content" class="column"><div class="section">
    	<?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
    	<?php print $back_link ?>
      <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
      <?php print render($title_prefix); ?>


<?php
global $base_url;
$uid = $node->uid;
$user_obj = user_load($uid);
$profile = profile2_load_by_user($user_obj); 

if (isset($user_obj->picture)) {
//var_dump($user_obj->picture);
$img = $user_obj->picture->filename;
} else {
$img = '../default-profile-image.jpg';
}
$img = $base_url . "/sites/default/files/pictures/" . $img;

echo "<div class='user-image'><img src=$img></div>";
?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>



      	<?php if(isset($node->field_subtitle['und'][0]['value'])) : ?>
      		<h2 class="event-subtitle">
	      		<?php echo $node->field_subtitle['und'][0]['value']; ?></h2>
      	<?php endif; ?>

	      <div id="single-event-stuff" class="free paid">
        <?php
        $status = '';
        $date = strtotime($node->field_date_time['und'][0]['value']);
        if($date < time()) {
          // If event date is in the past status should be "Tickets are no longer available"
          $status = 'Tickets are no longer available';
        } else {
          if(isset($node->field_sale_end_date['und'])) {
            $status = 'Tickets Still Available as of ' . date('m/d/y', strtotime($node->field_sale_end_date['und'][0]['value']));
          }
        }
        if($status) {
          ?>
          <div class="field field-name-field-sale-end-date field-type-datetime field-label-hidden"><span class="event-status">EVENT STATUS:</span>
          <?php echo $status; ?>
          </div>        
          <?php
        }
        ?>

			<?php if(isset($node->field_date_time['und'])): 
				$dateTime = date('l F j', strtotime($node->field_date_time['und'][0]['value'] . "-5 hours"));

	      if (date("I", strtotime($node->field_date_time['und'][0]['value'])) == 1) {
		      $dateTime = date('l F j', strtotime($node->field_date_time['und'][0]['value'] . "-4 hours"));
	      }

			?>
			<h3 class="event-day-heading"><?php echo $dateTime; ?></h3>
			<?php endif; ?>

			<?php if(isset($node->field_event_type['und']) && isset($node->field_event_type['und'])): ?>
			<div class="event-type">
			<img src="<?php echo file_create_url($node->field_event_type['und'][0]['taxonomy_term']->field_image['und'][0]['uri']); ?>" width="23" height="23" alt="">
			<?php
			echo $node->field_event_type['und'][0]['taxonomy_term']->name; ?>
			</div>
			<?php endif; ?>

			<div class="event-details">
			<?php if(isset($node->field_date_time['und'])): ?>
	      <div><?php

	      $firstTime = date('g:i A', strtotime($node->field_date_time['und'][0]['value'] . "-5 hours"));
	      $lastTime = date('g:i A', strtotime($node->field_date_time['und'][0]['value2'] . "-5 hours"));

	      if (date("I", strtotime($node->field_date_time['und'][0]['value'])) == 1) {
		      $firstTime = date('g:i A', strtotime($node->field_date_time['und'][0]['value'] . "-4 hours"));
	      }

	      if (date("I", strtotime($node->field_date_time['und'][0]['value2'])) == 1) {
		      $lastTime = date('g:i A', strtotime($node->field_date_time['und'][0]['value2'] . "-4 hours"));
	      }

	      if( $firstTime !== $lastTime) :
	      	echo $firstTime . " - " . $lastTime;
	      else :
	      	echo $firstTime;
	      endif;
	      ?></div>
	      <?php endif; ?>

	      <?php if(isset($node->field_location['und'][0])): ?>
	      <div class="event-date"><?php echo $node->field_location['und'][0]['thoroughfare'] . " " . $node->field_location['und'][0]['premise'] . "<br>" . $node->field_location['und'][0]['locality'] . " " . $node->field_location['und'][0]['administrative_area'] . " " . $node->field_location['und'][0]['postal_code']; ?></div>
			</div>
			<?php endif; ?>

			<?php if(isset($node->field_location_website['und'][0]['value'])) : ?>
			<div class="event-details">
				<br><?php echo "Event Link: <a class=\"website\" href='" . $node->field_location_website['und'][0]['value'] . "'>" . $node->title . "</a>"; ?>
	      </div>
			<?php endif; ?>
			<div class="categories">
	      <?php
		   if(isset($node->field_category['und'])) {
		      echo "<b>Categories: </b>";
		      for($i = 0; $i < count($node->field_category['und']); $i++) {
		      	echo "<span>" . $node->field_category['und'][$i]['taxonomy_term']->name;
		      	if($i == count($node->field_category['und']) - 1)
		      		echo "</span>";
		      	else
		      		echo ",</span>";
		      }
	      }
	      ?>
	      </div>
      </div>
      <?php if(isset($node->body['und'])): ?>
      <div class="event-content">
	      <?php echo $node->body['und'][0]['value']; ?>
      </div>
      <?php endif; ?>

      <?php if(!empty($ticket_price) || !empty($buy_ticket_link)): ?>
      <div class="field field-name-field-price field-type-commerce-price field-label-inline clearfix">        
          <?php print $ticket_price; ?>
          <?php print $buy_ticket_link; ?>
      </div>
		<?php endif; ?>

		<?php if(isset($node->field_image['und'])): ?>
      <div class="field field-name-field-image-2 field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="<?php echo image_style_url('single-event-2', $node->field_image['und'][0]['uri']); ?>" width="263" height="212" alt=""></div></div></div>
      <?php endif; ?>

      <?php if(isset($node->field_image_2['und'])): ?>
      	<div class="field field-name-field-image field-type-image field-label-hidden big-image"><div class="field-items"><div class="field-item even"><img src="<?php echo image_style_url('single-event-1', $node->field_image_2['und'][0]['uri']); ?>" width="550" height="325" alt=""></div></div></div>
      <?php endif; ?>

      <?php print $feed_icons; ?>
      <?php if(isset($node->field_location['und'])): ?>
      <?php $addr = $node->field_location['und'][0]['thoroughfare'] . ", " . $node->field_location['und'][0]['locality'] . " " . $node->field_location['und'][0]['administrative_area'] . " " . $node->field_location['und'][0]['postal_code']; ?>
      <?php else:
      	$addr = ""; ?>
      <?php endif; ?>


      <?php if($addr !== ""): ?>
      	<img src="http://maps.google.com/maps/api/staticmap?center=<?php echo urlencode($addr); ?>&zoom=16&size=263x212&sensor=false&markers=label:A|<?php echo urlencode($addr); ?>"
			alt="" class="free-google-map" />
		<?php endif; ?>
<?php

if(isset($node->field_phone2['und'])):
	$phone = $node->field_phone2['und'][0]['value'];
	$phone = format_phone($phone);
	if ($phone) :
		$phone = "Call $phone to make a reservation";
		echo "<div class=phone-reservation>$phone</div>";
	endif;
endif;

$obj = $profile['main'];
$socialLinks="";
if(isset($profile['main']->field_twitter['und'])):
try {
$social = $profile['main']->field_twitter['und'][0]['value'];
$socialName = explode("/", $social);
$socialName = array_pop($socialName);
$socialName = "@".$socialName;
$socialImage = 'twitter.jpg';
if ($social != "" && $social != "http://www.twitter.com/")
$socialLinks .= getSocialUrl($social, $socialName, $socialImage, 'twitter');
} catch (MyException $e) {}
endif;

if(isset($profile['main']->field_facebook['und'])):
try {
	if (isset($profile['main']->field_facebook['und'])) {
		$social = $profile['main']->field_facebook['und'][0]['value'];
		$socialName = explode("http://", $social);
		$socialName = array_pop($socialName);
		$socialName = explode("https://", $social);
		$socialName = array_pop($socialName);
		$socialName = explode("www.", $social);
		$socialName = array_pop($socialName);
		$socialImage = 'facebook.jpg';
		if ($social != "" && $social != "http://www.facebook.com/")
		$socialLinks .= getSocialUrl($social, "facebook", $socialImage, 'facebook');
	}
} catch (MyException $e) {}
endif;

try {
	if (isset($profile['main']->field_company_blog['und'])) {
		$social = $profile['main']->field_company_blog['und'][0]['value'];
		$socialName = explode("http://", $social);
		$socialName = array_pop($socialName);
		$socialName = explode("https://", $social);
		$socialName = array_pop($socialName);
		$socialName = explode("www.", $social);
		$socialName = array_pop($socialName);
		$socialImage = 'blog.jpg';
		if ($social != "")
		$socialLinks .= getSocialUrl($social, "Blog", $socialImage, 'blog');
	}
} catch (MyException $e) {}

if ($socialLinks!='') {
echo "<h4 class='social-heading'>Connect with $node->name</h4>";
echo '<div class="social">';
echo $socialLinks;
echo "</div>";
echo "<div style=clear:left;> </div>";
$socialLinks="";
}

try {
	if (isset($profile['main']->field_yelp['und'])) {
		$social = $profile['main']->field_yelp['und'][0]['value'];
		$socialName = "Yelp";
		$socialImage = 'yelp.jpg';
		if ($social != "")
		$socialLinks .= getSocialUrl($social, $socialName, $socialImage, 'yelp');
	}
} catch (MyException $e) {}

try {
	if (isset($profile['main']->field_opentable['und'])) {
		$social = $profile['main']->field_opentable['und'][0]['value'];
		$socialName = "OpenTable";
		$socialImage = 'opentable.jpg';
		if ($social != "")
		$socialLinks .= getSocialUrl($social, $socialName, $socialImage, 'opentable');
	}
} catch (MyException $e) {}

try {
	if (isset($profile['main']->field_zagat['und'])) {
		$social = $profile['main']->field_zagat['und'][0]['value'];
		$socialName = "Zagat";
		$socialImage = 'zigat.jpg';
		if ($social != "")
		$socialLinks .= getSocialUrl($social, $socialName, $socialImage, 'zagat');
	}
} catch (MyException $e) {}

try {
  if (isset($profile['main']->field_city_eats['und'])) {
    $social = $profile['main']->field_city_eats['und'][0]['value'];
    $socialName = "City Eats";
    $socialImage = 'cityeats.png';
    if ($social != "")
    $socialLinks .= getSocialUrl($social, $socialName, $socialImage, 'cityeats');
  }
} catch (MyException $e) {}

if ($socialLinks!='') {
echo "<h4 class='social-heading'>What others are saying about $node->name</h4>";
echo '<div class="social">';
echo $socialLinks;
echo "</div>";
echo "<div style=clear:left;> </div>";
unset($socialLinks);
}
echo "<div class='more_posts_by-wrapper'>";
echo "<h3 class='more_posts_by_heading'>Other Events by $node->name</h3>";

$nid = 0;

if (!empty($node->nid)) $nid = $node->nid;

$result = db_query("SELECT n.nid, n.title, dt.field_date_time_value, fm.uri, fm.filename
		FROM {node} n
		INNER JOIN {field_data_field_date_time} dt
		ON n.nid = dt.entity_id
		INNER JOIN {field_data_field_image} img
		ON n.nid = img.entity_id
		INNER JOIN  {file_managed} fm
		ON img.field_image_fid = fm.fid
		WHERE n.uid = :uid AND n.nid <> :nid
		ORDER BY dt.field_date_time_value DESC
		LIMIT 3 ", array(':uid'=>$uid, ':nid'=>$nid));
//var_dump($result);
foreach ($result as $row) {
echo "<div class=more_posts_by>";
$href = $base_url . "/node/".$row->nid;

$datetime = new DateTime($row->field_date_time_value, new DateTimeZone('UTC'));
$datetime->setTimezone(new DateTimeZone(drupal_get_user_timezone()));
$date = $datetime->format('l, F j');
$time = $datetime->format('\a\t g:i a');
echo "<a href=$href>";
echo "<img src=" . image_style_url('free-event-thumbnail', $row->filename) . " alt=''>";
echo "</a>";
echo "<div class=datetime>$date <span class='time'>$time</span></div>";
echo "<a href=$href>";
echo "<h3>".$row->title."</h3>";
echo "</a>";
echo "</div>";
}
echo "</div>";

?>

</div>
</div>
</div>
    </div></div> <!-- /.section, /#content -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->

  <?php if ($page['triptych_first'] || $page['triptych_middle'] || $page['triptych_last']): ?>
    <div id="triptych-wrapper"><div id="triptych" class="clearfix">
      <?php print render($page['triptych_first']); ?>
      <?php print render($page['triptych_middle']); ?>
      <?php print render($page['triptych_last']); ?>
    </div></div> <!-- /#triptych, /#triptych-wrapper -->
  <?php endif; ?>

  <div id="footer-wrapper"><div class="section">

    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
      <div id="footer-columns" class="clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->