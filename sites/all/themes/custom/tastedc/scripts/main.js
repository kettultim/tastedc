jQuery(document).ready(function($){

	$("#block-block-4 .event-type a:not(#all-events)").each(function() {
		var img = eventType( $(this).text() ).replace( $(this).text(), "" );
		$(this).append( img );
	});

	// Google Maps
		if ($(".free-google-map").length !== 0) {
			var geocoder = new google.maps.Geocoder();
			$('.free-google-map').replaceWith("<div id='map_canvas'> </div>");

			address = $('.event-date').text().replace("Location: ", "");
			if (address.length > 10) {
				geocoder.geocode({ 'address': address}, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK)
					{
						var longitude = results[0].geometry.location.lng();
						var latitude = results[0].geometry.location.lat();

						$('#map_canvas').gmap().bind('init', function(ev, map) {
							$('#map_canvas').gmap('addMarker', {'position': latitude + "," + longitude, 'bounds': true}).click(function() {
								document.location = "http://maps.google.com/maps?q=" + address;
							});
							$('#map_canvas').gmap('option', 'zoom', 10);
						});
						showDefaultMap = false;
					} else {
						$("#map_canvas").addClass("needs-map");
					}
				});
			} else {
			$("#map_canvas").addClass("needs-map");
		}
	}

	function formatHomePage() {
		// Sometimes fixed
		$("#block-block-4").addClass('sticky');
		$("#quicktabs-date").addClass('sticky');
		if($(".sticky").length !== 0) {
			var sticky = $( ".sticky" );
			var originalStickyTop = sticky.offset().top;
			var view = $( window );
			view.bind( "scroll resize", function() {
				var viewTop = view.scrollTop();
				if ( (viewTop > originalStickyTop) && !sticky.is( ".sticky-fixed" ) ) {
					sticky.removeClass( "sticky-absolute" ).addClass( "sticky-fixed" );
				} else if ( (viewTop <= originalStickyTop) && sticky.is( ".sticky-fixed" ) ) {
					sticky.removeClass( "sticky-fixed" ).addClass( "sticky-absolute" );
				}
			});
		}
		$(".view-events .date-display-single").each(function(){
			var str = $(this).text();
			str = formatDate(str);
			$(this).html(str);
		});

		$(".view-events .date-display-start, .view-events .date-display-end").each(function(){
			var str = $(this).text();
			str = formatDate(str);
			$(this).html(str);
		});

		$(".view-events .views-row").click(function(){
			window.location = $(this).find("a").attr("href");
		});
	}


	// All pages
	$(".submitted span").each(function(){
		var str = $(this).html();
		var exploded = str.split('</a>');
		str = exploded[0] + '</a>';
		$(this).html(str);
	});

	// Featured Items on Homepage - Header
	$(".view-homepage-thumbnails-paid-events .node").append("<div class='purchase-tickets'>PURCHASE TICKETS</div>");
	$(".view-homepage-thumbnails-paid-events .node").hover(function(){
		$(this).css("backgroundColor","#c31d0d");
	},function(){
		$(this).css("backgroundColor","#393939");
	});
	$(".view-homepage-thumbnails-paid-events .date-display-single").each(function(){
		var str = $(this).text();
		$(this).html(formatDate(str));
	});
	$(".view-homepage-thumbnails-paid-events .views-row .node").click(function(e) {
		window.location = $(this).find("h2 a").attr("href");
	});

	// Featured Items on Homepage - Content
	$(".view-events .node-paid-event-listing").append("<div class='featured'><div class='text'>FEATURED</div><span class='image'> </span></div>");
	$(".view-events .node-paid-event-listing").hover(function(){
		$(this).find(".featured .text").show();
		$(this).find(".featured").css("backgroundColor","#740e0e");
		$(this).css("backgroundColor","#c31d0d");
	},function(){
		$(this).find(".featured .text").hide();
		$(this).find(".featured").css("backgroundColor","");
		$(this).css("backgroundColor","#393939");
	});

	// Listing Page - All
	$(".more_posts_by").click(function(e){
		document.location = $(this).find("a").attr("href");
	});

	if ($(".node-type-event-listing").length != 0) {

		// Listing Page - All
		$(".field-name-field-sale-end-date .date-display-single").prepend("<span class='event-status'>EVENT STATUS:</span> Tickets Still Available as of ");
		$(".field-name-field-price .field-label").hide();
		var cost = $(".field-name-field-price .field-items div").text();
		cost = "<h3>Cost: "+cost+" / person</h3>";
		 $(".field-name-field-price .field-items div").html(cost);

		// Listing Page - Paid
		if ($(".field-name-field-logo").length != 0) {
			$("#content").addClass("haslogo");
		}

		if ($(".field-name-field-image-2").length != 0) {
			$(".field-name-field-image").addClass("big-image");
			$("#content").addClass("has-big-image");
		} else {
			$("#content").addClass("has-no-big-image");
		}
		$(".node-type-event-listing .field-name-field-date-time").each(function(){
			var str = $(this).text();
			str = formatDate(str);
			$(this).html(str);
		});
		$(".more_posts_by .datetime").each(function(){
			var str = $(this).text();
			str = formatDate(str);
			$(this).html(str);
		});
	}

	// Tabs
	$(".front #block-system-main").before("<div id='quicktabs-date'></div>");
	var quicktabs = $("#quicktabs-date");
	quicktabs.append("<h2>Date: </h2><ul class='quicktabs-tabs'><li><a href='#' id='all-time' class='all-time-link'>All Time</a></li><li class='last'><a href='#' id='choose-date' class='choose-date-link'>Choose Date</a></li></ul>");

	$("#choose-date").prepend("<input id='date-placeholder'/>");
	$("#date-placeholder").datepicker({
		numberOfMonths: 2,
		defaultDate: +7,
		firstDay: 1
	});

    $(document).ready(function(){

    	var date_filter = readCookie("tdc_filter_type");
        //alert(date_filter);   	
      $('ul.quicktabs-tabs li').removeClass("active");
      
      if( date_filter == 'choose-date' ) {
        var date1;
        var date2;
        var trigger = false;
        if($("#edit-field-date-time-value-1-value-date").val() == '' || $("#edit-field-date-time-value-1-value-date").val() == null) {
          date1 = readCookie("tdc_min_date");
          $("#edit-field-date-time-value-1-value-date").val(date1);
          trigger = true;
        } else {
          date1 = $("#edit-field-date-time-value-1-value-date").val();
        }
        if($("#edit-field-date-time-value-2-value-date").val() == '' || $("#edit-field-date-time-value-2-value-date").val() == null) {
          date2 = readCookie("tdc_max_date");
          $("#edit-field-date-time-value-2-value-date").val(date2);
          trigger = true;
        } else {
          date2 = $("#edit-field-date-time-value-2-value-date").val();
        }
        if(date1 == '' || date2 == '' || date1 == null || date2 == null) {
          createCookie('tdc_min_date', "", 1);
          createCookie('tdc_max_date', "", 1);
          createCookie('tdc_filter_type', 'All Time', 1);
          date_filter = 'All Time';          
        } else {
          $('.choose-date-link').parent().addClass("active");
          var firstday = new Date();        
          var lastday = new Date();
          firstday.setTime(Date.parse(date1));
          lastday.setTime(Date.parse(date2));          
          var longMonths = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
          $("#choose-date").text("Week of " + longMonths[firstday.getMonth()] + " " + firstday.getDate() + " - " + lastday.getDate());
          $("#choose-date").prepend("<input id='date-placeholder'/>");
          $("#date-placeholder").datepicker({
            numberOfMonths: 2,
            firstDay: 1
          });
          $("#date-placeholder").change(function() {
            sortByChooseDate();
          });
          if(trigger) $("#edit-submit-events").trigger("click");          
        }                       
      }        
        
    	if( date_filter == "All Time" ) {
    	  $('.all-time-link').parent().addClass("active");
    	}
    	else if ( date_filter == "" || date_filter == null ) {
    	  $('ul.quicktabs-tabs li .all-time-link').parent().addClass("active");
    	}

    });

	$("#quicktabs-date a").live("click", function() {

		var minDate = $("#edit-field-date-time-value-1-value-date");
		var maxDate = $("#edit-field-date-time-value-2-value-date");

		var d = new Date();


		$("#quicktabs-date a").not(this).parent().removeClass("active");
		$(this).parent().addClass("active");

		if($(this).text() == "All Time") {
			minDate.val("");
			maxDate.val("");
			createCookie('tdc_min_date', "", 1);
			createCookie('tdc_max_date', "", 1);
			createCookie('tdc_filter_type', $(this).text(), 1);
		}

		if( $(this).attr("id") == "choose-date") {
			$('#date-placeholder').datepicker("show");      
		}

		if( $(this).attr("id") !== "choose-date") {
			$("#edit-submit-events").trigger("click");
		}
		return false;
	});

	// Ajax for block-block-4
	$('#block-block-4 a').click(function(event) {
		var form = $("views-exposed-form-events-page");
		var category = $("#edit-category");
		var type = $("#edit-type");
		var val = $.trim($(this).text());

		// Contextual link override
		if( $(this).hasClass("contextual-links-trigger") ) {
			return true;
		}

		$(this).toggleClass("selected");

		// All Events link
		if( $(this).attr('id') == "all-events" ) {
			$("#block-block-4 .event-type a:not(#all-events)").removeClass("selected");
			type.val("");
		}

		// Events Type
		if( $(this).parents(".event-type").length == 1 &&
			$(this).attr('id') != "all-events" &&
			$(this).hasClass("contextual-links-trigger") != true ) {

			$("#all-events").removeClass('selected');
			if( type.val().indexOf(val) == -1 ) {
				type.val(type.val() + val + ",");
			}
			else {
				type.val(type.val().replace(val + ",", "") );
			}
		}

		// All Categories link
		if( $(this).attr('id') == "all-categories" ) {
			$("#block-block-4 .event-category a:not(#all-categories)").removeClass("selected");
			category.val("");
		}

		// Event Category
		if( $(this).parents(".event-category").length == 1 && $(this).attr('id') != "all-categories" ) {
			$("#all-categories").removeClass('selected');
			if( category.val().indexOf(val) == -1 ) {
				category.val(category.val() + val + ",");
			}
			else {
				category.val(category.val().replace(val + ",", "") );
			}
		}

		/*if($("#block-block-4 a").is('.selected') == false ) {
			$("#all-events").addClass("selected");
		}*/

		$("#edit-submit-events").trigger("click");
		event.preventDefault();
		return false;
	});

	$("#date-placeholder").change(function() {
		sortByChooseDate();
	});

	function sortByChooseDate() {
		var minDate = $("#edit-field-date-time-value-1-value-date");
		var maxDate = $("#edit-field-date-time-value-2-value-date");
		var longMonths = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		var curr;
		try {
			curr = new Date($("#date-placeholder").val());
		} catch(ex) {}

		if(isDate(curr)) {
			var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
			var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));
      var sFirstDate = firstday.getFullYear() + "-" + (firstday.getMonth() + 1) + "-" + firstday.getDate();
      var sLastDate = lastday.getFullYear() + "-" + (lastday.getMonth() + 1) + "-" + lastday.getDate();      
			minDate.val(sFirstDate);
			maxDate.val(sLastDate);
			$("#choose-date").text("Week of " + longMonths[firstday.getMonth()] + " " + firstday.getDate() + " - " + lastday.getDate());

			$("#choose-date").prepend("<input id='date-placeholder'/>");
			$("#date-placeholder").datepicker({
				numberOfMonths: 2,
				firstDay: 1
			});

			$("#date-placeholder").change(function() {
				sortByChooseDate();
			});     
      
      createCookie('tdc_min_date', $.datepicker.formatDate('yy-mm-dd', firstday), 1);
      createCookie('tdc_max_date', $.datepicker.formatDate('yy-mm-dd', lastday), 1);
      createCookie('tdc_filter_type', 'choose-date', 1);      
      
			$("#edit-submit-events").trigger("click");
		}
	}

	Drupal.behaviors.mybehavior = {
		attach: function (context, settings) {
			formatHomePage();
		}
	};
	$('#views-exposed-form-events-page').ajaxStart(function(){
		var site_base_path = Drupal.settings.basePath;
		$("body").append("<div id='loader'><img src='" + site_base_path + "sites/all/themes/custom/tastedc/images/loader.gif'></div>");
			});
	$('#views-exposed-form-events-page').ajaxSuccess(function(){
		$("#loader").remove();
	});

	$("#campaignmonitor-subscribe-form #edit-submit").val("Subscribe to TasteDC");

	formatHomePage();
});

function formatDate(str) {
	var output;
			var exploded = str.replace(" - ", " at ");
			exploded = exploded.replace(" at ", "<b class='timestart'> at ");
			output = '<span class="datestart">' + exploded + '</b></span>';
		return output;
}

function isDate(val) {
	var d = new Date(val);
	return !isNaN(d.valueOf());
}

function eventType(val) {
	var fullpath = base_url + '/sites/default/files/styles/thumbnail/public/'
	var types = new Array();
	var img;
	types['Food Tours'] = 'FoodToursIconBlack.png';
	types['Tastings'] = 'icon1.png';
	types['Cooking Classes'] = 'icon2.png';
	types['Festivals'] = 'icon3.png';
	types['Dinner Events'] = 'icon4.png';
	img = '<img src="'+fullpath+types[val]+'"> ' + val;
	return img;
}

/**
 * COOKIES
 */

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	//return null;
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

