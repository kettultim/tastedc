/**
 * @file
 * ga-events.js
 * GA Events tracking.
 */

(function ($) {

  Drupal.behaviors.gaEventsTracking = {
    attach: function(context) {

      // Track purchase button clicks
      $('.node-type-event-listing #content a.purchase-tickets', context).click(function() {
        _gaq.push(['_trackEvent', 'Buttons', 'Click', 'Purchase Ticket']);
      });

      $('.node-type-event-listing #content .event-details a.website', context).click(function() {
        _gaq.push(['_trackEvent', 'Social profiles', 'Click', 'website']);
      });

      var socialLinks = new Array('facebook', 'twitter', 'blog', 'yelp', 'opentable', 'zagat', 'cityeats');

      for (var i = 0; i < socialLinks.length; i++) {

        $('#content .social-links.' + socialLinks[i] + ' a', context).click(function(){
          
          var type = $.trim($(this).parent('.social-links').attr('class').replace('social-links', ''));          
          _gaq.push(['_trackEvent', 'Social profiles', 'Click', type]);        
        });
      }
    }
  };
}(jQuery));