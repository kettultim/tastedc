<?php

/**
 * Implementation of hook_drush_help().
 */
function tdc_ga_drush_help($section) {
  switch ($section) {
    case 'drush:tdc-ga-import':
      return dt('Import Google Analytics data for tastedc.com.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function tdc_ga_drush_command() {
  $items['tdc-ga-import'] = array(
    'callback' => 'tdc_ga_import_ga_data',
    'description' => 'Import Google Analytics data for tastedc.com.',
  );
  return $items;
}

