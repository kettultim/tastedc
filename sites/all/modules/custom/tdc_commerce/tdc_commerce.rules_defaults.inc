<?php
/**
 * @file
 * tdc_commerce.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function tdc_commerce_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_order_email_invoice'] = entity_import('rules_config', '{ "rules_commerce_order_email_invoice" : {
      "LABEL" : "Commerce Order Email Invoice",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_invoice_receipt", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "DO" : [
        { "commerce_invoice_receipt_action_mail" : {
            "commerce_order" : [ "commerce_order" ],
            "to" : "[commerce-order:owner] \\u003C[commerce-order:mail]\\u003E",
            "subject" : "[site:name]: Order #[commerce-order:order-id]"
          }
        }
      ]
    }
  }');
  $items['rules_commerce_order_email_invoice_admin'] = entity_import('rules_config', '{ "rules_commerce_order_email_invoice_admin" : {
      "LABEL" : "Commerce Order Email Invoice Admin",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_invoice_receipt", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "DO" : [
        { "commerce_invoice_receipt_action_mail" : {
            "commerce_order" : [ "commerce_order" ],
            "to" : "wine@tastedc.com",
            "subject" : "[site:name]: Order #[commerce-order:order-id]"
          }
        }
      ]
    }
  }');
  $items['rules_send_email_alerts_for_purchased_events'] = entity_import('rules_config', '{ "rules_send_email_alerts_for_purchased_events" : {
      "LABEL" : "Send email alerts for purchased events",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_node_checkout" ],
      "ON" : [ "commerce_node_checkout_published_node" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "event_listing" : "event_listing" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "TasteDC - Thanks for purchasing an event!",
            "message" : "Hi!\\r\\n\\r\\nThanks for purchasing an event for TasteDC.com:\\r\\n[node:title]\\r\\n[node:url]\\r\\n\\r\\nOur editors will review your event and be in touch soon.\\r\\n\\r\\nThanks!",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_unpublish_purchased_nodes'] = entity_import('rules_config', '{ "rules_unpublish_purchased_nodes" : {
      "LABEL" : "Unpublish purchased events",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "commerce_node_checkout" ],
      "ON" : [ "commerce_node_checkout_published_node" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "event_listing" : "event_listing" } }
          }
        }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  return $items;
}
