<?php

/**
 * @file
 * Drush commands for site.
 */

/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 */
function tdc_helper_drush_command() {
  return array(
    'go-stage' => array(
      'description' => dt('Does everything drush go-local does, with some overrides for the stage server.'),
      'aliases' => array('gostage'),
    ),
    'go-local' => array(
      'description' => dt('Puts your site in local development mode.'),
      'aliases' => array('golocal'),
    ),
  );
}

/**
 * Drush command callback. Does everything drush_tdc_helper_go_local()
 * does but makes a few specific changes we need on the dev server.
 */
function drush_tdc_helper_go_stage() {
  // We will flush caches at the end.
  drush_tdc_helper_go_local(FALSE);

  module_disable(array('devel'));

  // Turn on CSS aggregration to avoid hitting IE's CSS limit.
  variable_set('preprocess_css', "1");
  // Enable page cache so custom caching kicks in.
  variable_set('cache', 1);
  drush_log(dt('Page cache and CSS preprocessing enabled.'), 'ok');

  drupal_flush_all_caches();
  drush_log(dt('All caches cleared. Site ready for development!'), 'ok');

  return TRUE;
}

/**
 * Drush command callback. Put the site in a state friendly for development.
 *
 * @param $reset
 *   True to flush the cache at the end of the changes.
 */
function drush_tdc_helper_go_local($reset = TRUE) {
  // Enable dev friendly modules. stage_file_proxy will download files from dev
  // if they don't exist locally. Add this to your secret.settings.php file to
  // point your local to dev:
  // $conf['stage_file_proxy_origin'] = 'http://POPUPUSERNAME:POPUPPASSWORD@dev.wsgc.gotpantheon.com';
  $dev_modules_to_enable = array('devel', 'reroute_email', 'dblog', 'update', 'views_ui');
  module_enable($dev_modules_to_enable, TRUE);
  foreach($dev_modules_to_enable as $module) {
    if(module_exists($module)) {
      drush_log(dt("@module enabled.", array('@module' => $module)), 'ok');
    } else {
      drush_log(dt("@module is NOT enabled!", array('@module' => $module)), 'error');
    }
  }
  
  // Disable production modules.
  $disable = array(
    'cdn',
    'securepages',
  );
  module_disable($disable);
  drush_log(dt('Modules disabled: @modules', array('@modules' => implode(', ', $disable))), 'ok');

  // Make anonymous users see devel messages
  $anonymous_role = user_role_load_by_name('anonymous user');
  user_role_grant_permissions($anonymous_role->rid, array('access devel information'));
  $authenticated_role = user_role_load_by_name('authenticated user');
  user_role_grant_permissions($authenticated_role->rid, array('access devel information'));

  drush_log(dt('All users can now see devel messages.'), 'ok');

  // Make sure the rerouting of email is turned on
  if(module_exists('reroute_email')) {
    variable_set('reroute_email_enable', 1);
    $reroute_email_address = drush_prompt(dt('What email address would you like to reroute emails to?'), 'justin.emond@gmail.com');
    variable_set('reroute_email_address', $reroute_email_address);
    drush_log(dt("Email is being rerouted to @reroute_email_address@.", array('@reroute_email_address' => $reroute_email_address)), 'ok');
  } else {
    drush_log(dt('Emails will be sent to users!'), 'warning');
  }

  // Set some dev-friendly variables
  variable_set('cache', "0");
  variable_set('preprocess_js', "0");
  variable_set('preprocess_css', "0");
  variable_set('block_cache', "0");
  variable_set('error_level', "2");
  variable_set('devel_error_handler', "1");
  variable_set('page_compression', "0");
  drush_log(dt('Page cache, page compression, JS optimization, and CSS optimization disabled.'), 'ok');

  if($reset) {
    // Finish up with a cache flush
    drush_log('Clearing caches...', 'ok');
    drupal_flush_all_caches();
    drush_log(dt('All caches cleared.'), 'ok');
  }

  drush_log(dt('Site ready for development!'), 'ok');
}
