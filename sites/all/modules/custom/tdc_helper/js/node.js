/**
 * @file
 * JS Helper
 */

(function ($) {

  Drupal.behaviors.tdcHelper = {
    attach: function(context) {

      $('#edit-field-sale-end-date div[class*=form-item-field-sale-end-date-und-] label', context).text(Drupal.t('Last Day to Buy Tickets'));
      $('#edit-field-promote-event .form-type-radios .form-radios .form-item:first label').css('font-weight', 'bold');

      var previousLocation = $('#edit-field-previous-location select', context);
      if ( previousLocation.val() == '_none' ) {
        previousLocation.attr('disabled', 'disabled');
        $('#locationtype1').attr('checked', 'checked');
      }
      else {
        previousLocation.removeAttr('disabled');
        $('#locationtype0').attr('checked', 'checked');
      }

      previousLocation.change(function(){
        if ( $(this).val() == '_none' ) {
          $(this).attr('disabled', 'disabled');
          $('#locationtype1').attr('checked', 'checked');
        }
        else {
          $(this).removeAttr('disabled');
          $('#locationtype0').attr('checked', 'checked');
        }
      });

      $('input[name=locationtype]', context).change(function(){

        if ( $(this).val() == 0 ) {
          previousLocation.removeAttr('disabled');
          $('#edit-field-location input, #edit-field-location select').attr('disabled', 'disabled');


        }
        else {
          previousLocation.attr('disabled', 'disabled');
          previousLocation.val('_none').attr('selected', 'selected');
          $('#edit-field-location input, #edit-field-location select').removeAttr('disabled');
        }

      });

      $('#edit-field-promote-event label:first', context).hide();

      var promoteEvent = $('#edit-field-promote-event input');

      if ( promoteEvent.filter(':checked').val() == 0 ) {
        $('#paid-event-form-elements').show();
      }
      else {
        $('#paid-event-form-elements').hide();
      }

      promoteEvent.change(function(){
        if ( $(this).val() == 0 ) {
          $('#paid-event-form-elements').show();
          var conetntHeight = $('#content .region-content .content').height();
          var heightOffset = 300;
          $('#sidebar').height(conetntHeight + heightOffset);
          $('.page-node-add #sidebar').height(conetntHeight + heightOffset);
        }
        else {
          $('#paid-event-form-elements').hide();
          var conetntHeight = $('#content .region-content .content').height();
          var heightOffset = 300;
          $('#sidebar').height(conetntHeight + heightOffset);
          $('.page-node-add #sidebar').height(conetntHeight + heightOffset);
        }
      });

    }
  };
}(jQuery));

/**
 * Show fields for Paid events
 */
function _tdcHelperShowFields(context) {
  (function ($) {
    //$('#edit-body, #edit-field-subtitle, #edit-field-image-2, #edit-field-buy-ticket-link, #edit-field-sale-end-date', context).show();
  }(jQuery));
}

/**
 * Hide fields for Paid events
 */
function _tdcHelperHideFields(context) {
  (function ($) {
   // $('#edit-body, #edit-field-subtitle, #edit-field-image-2, #edit-field-buy-ticket-link, #edit-field-sale-end-date', context).hide();
  }(jQuery));
}