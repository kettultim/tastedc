<?php if ( $has_access ) : ?>

  <h2><?php print $event_title; ?></h2>

  <div class="event-submitted">
  	<?php print $event_submitted; ?>
  </div>

  <div class="event-status<?php print $event_status ? ' esuccess' : ' ewarning'  ?>">  
    <p><?php print $event_status ? 'Published' : ' Under Review'  ?></p>
  </div>

  <div class="divider"><span></span></div>

  <?php print $tickets_available_form; ?>

  <div class="widget chartWrapper">

      <div class="whead"><h6><?php print t('Analytics Overview'); ?></h6><div class="clear"></div></div>
      <div class="body">

        <?php if ($featured_event): ?>

          <?php if ( count($stats) ) : ?>

            <div id="event_summary" class="fluid">

              <div class="grid3 stat-labels-wrapper">

                <p class="stats-labels">
                  <span><?php print t('page views'); ?></span>
                </p>
                <p class="stats-labels">
                  <span><?php print t('"purchase tickets" click-throughs'); ?></span>
                </p>
              </div>

              <div class="grid9">

                <?php foreach ( $stats['bars'] as $type => $bar ) : ?>

                <div class="stats-total">
                  <div class="<?php print $type; ?>">
                    <div class="bar-chart-width"><div class="histogram" style="width: <?php print $bar['width']; ?>%; "></div></div>
                    <div><span class="count"><?php print $bar['count']; ?></span></div>
                  </div>
                </div>

                <?php endforeach; ?>

              </div>
               
              <div class="fluid"><div class="grid12"><div class="divider"></div></div></div>

              <div class="fluid site-clicks">

                <?php if ( count($stats['sites']) ) : ?>

                  <?php foreach ($stats['sites'] as $site) : ?>

                    <div class="site-item-wrapper" style="width: <?php print (100 / count($stats['sites']) - 0.5 ); ?>%;">

                      <div class="stat"><?php print $site['clicks']; ?></div>
                      <div class="name"><?php print $site['name']; ?></div>

                    </div>

                  <?php endforeach; ?>

                <?php else: ?>
                  <?php print t('No Google Analytics data to show. The data is imported every hour.'); ?>
                <?php endif; ?>

            </div>
             
            <div class="fluid"><div class="grid12"><div class="divider"></div></div></div>

          </div>

          <?php else : ?>
            <?php print t('No Google Analytics data to show. The data is imported every hour.'); ?>
            <div class="fluid"><div class="grid12"><div class="divider"></div></div></div>
          <?php endif; ?>

        <?php else: ?>
            <div class="nNote nInformation" style="margin-top: 5px;">
                <p>
                  <?php print t('Event Campaign Report is for Featured Events Only'); ?>
                </p>
            </div>          
          <div class="fluid"><div class="grid12"><div class="divider"></div></div></div>
        <?php endif; ?>

  		  <?php print $event_details; ?>

      </div>
  </div>



  <div class="widget">
          
      <div class="whead"><h6><?php print t('Traffic Source Analytics'); ?></h6><div class="clear"></div></div>
      <div class="body">

  		<div class="fluid">

        <?php if ($featured_event): ?>

          <?php if ( !empty($top_domains_table) || !empty($top_keyword_searches_table) ) : ?>

    		    <div class="grid6">
    		        <div class="body">
    		        	<strong><?php print t('Top Domains:'); ?></strong>
                  <?php print $top_domains_table; ?>

    		        </div>
    		    </div>
    		    
    		        
    		    <div class="grid6">
    		        <div class="body">
                  <strong><?php print t('Top Keyword Searches:'); ?></strong>
                  <?php print $top_keyword_searches_table; ?>
    		        </div>
    		    </div>

          <?php else : ?>
            <?php print t('No Google Analytics data to show. The data is imported every hour.'); ?>
          <?php endif; ?>

    		    <div class="clear"></div>
        <?php else: ?>
            <div class="nNote nInformation" style="margin-top: 0px;">
                <p>
                  <?php print t('Event Campaign Report is for Featured Events Only'); ?>
                </p>
            </div>                    
        <?php endif; ?>
  		    
  		</div>

      </div>

      <div class="clear"></div>
      
  </div>

  <div class="divider"><span></span></div>

  <div class="events-stats-footer fluid">
          
      <div class="grid3">

        <div class="fs1 iconb" data-icon="" style="font-size: 1.5em; display: inline-block; margin-right: 3px;"></div>
        <?php print l('Support / Contact Us', 'mailto:wine@tastedc.com'); ?>        

      </div>
          
      <div class="grid9">

        <a href="<?php print $event_url; ?>" target="_blank" title="" class="action-button sideB bLightBlue buttonL">
        	<span class="icos-imac"></span><span><?php print t('Preview In Browser'); ?></span>
        </a>
        <a href="<?php print $edit_event_url; ?>" title="" class="action-button sideB bLightBlue buttonM">
          <span class="icos-pencil"></span><span><?php print t('Edit Event'); ?></span>
        </a>
        <a href="<?php print $use_as_template_url; ?>" title="" class="action-button sideB bLightBlue buttonL">
          <span class="icos-documents"></span><span><?php print t('Use Event As Template'); ?></span>
        </a>

      </div>

      <div class="clear"></div>
      
  </div>

<?php else : ?>

  <?php print t("You don't have access to this event."); ?>

<?php endif; ?>