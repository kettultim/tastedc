<?php if (empty($active_featured_event_count)): ?>
<div class="nNote nInformation" style="margin-top: 0px;">
    <p>
      <?php print t('Event Campaign Report is for Featured Events Only'); ?>
    </p>
</div>
<?php endif ?>
<div class="widget chartWrapper campaign-stats-overview">
  <div class="whead">
    <h6>Campaign Stats Overview</h6>
    <div class="rightTabs">
     <ul class="tabs" id="report-tabs">      
      <li><a href="#this-week-reports" data-toggle="tab" class="">This Week</a></li>      
      <li><a href="#this-month-reports" data-toggle="tab" class="">This Month</a></li>      
      <li><a href="#ytd-reports" data-toggle="tab" class="activeTab">YTD</a></li>
    </ul>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>
<div class="body">

  <div class="tab-content">

    <?php foreach ($stats as $key => $values): ?>

    <div class="tab-pane fluid" style="display: none;" id="<?php print $key ?>">

      <div class="grid3 stats-summary">
        <h6><?php print $values['events']?> Events this <?php print $values['type']?></h6>
        <div class="divider"></div>
        <p class="stats-total">
          <span><?php print $values['pageviews'] ?></span> Page Views
        </p>
        <p class="stats-total">
          <span><?php print $values['purchases'] ?></span> Purchase Click-Throughs
        </p>
      </div>

      <div class="grid9 widget chartWrapper">   
        <div class="whead"><h6><?php print $values['chart']['title'] ?></h6><div class="clear"></div></div> 
        <div class="body">
        <div class="chart">
          <?php print $values['chart']['graph'] ?>
        </div>
      </div>
      </div>

    </div>
    <?php endforeach; ?>

  </div>

</div>
</div>