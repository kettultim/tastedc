<div class="widget">
	<div class="whead"><h6>Campaign Summaries</h6><div class="clear"></div></div>

	<div id="dynamic_wrapper" class="dataTables_wrapper" role="grid">

		<?php print $summaries; ?>                

		<div class="fg-toolbar tableFooter">
			<div class="dataTables_info" id="dynamic_info">
				<?php print $pager_summary; ?>
			</div>
			<div class="dataTables_paginate paging_full_numbers" id="dynamic_paginate">
				<?php print $pager; ?>
			</div>
		</div>
	</div> 	
</div>