<?php 

/**
 * Preprocess for campaign-summary-block.tpl.php
 */
function tdc_vendor_preprocess_campaign_summary_block(&$vars) {
  global $user;

  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');

  $query = db_select('node', 'n')->extend('PagerDefault'); 
  $query->join('field_data_field_date_time', 'dt', 'dt.entity_id = n.nid AND dt.revision_id = n.vid');
  $query->join('field_data_field_promote_event', 'pe',
    'pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0');
  $query->join('tdc_ga_stats', 'tgs', "n.nid = tgs.nid AND tgs.type = 'total'");
  $query->fields('n', array('nid', 'title'));
  $query->addExpression('CASE WHEN dt.field_date_time_value < NOW() THEN 1 ELSE 0 END', 'is_past');
  $query->fields('tgs', array('pageviews', 'website', 'facebook', 
      'twitter', 'blog', 'yelp', 'zagat', 'opentable', 'cityeats'));
  $query->condition('n.uid', $user->uid);
  $query->condition('n.type', 'event_listing');
  $query->condition('n.status', 1);
  $query->orderBy('tgs.pageviews', 'DESC');
  $query->orderBy('dt.field_date_time_value', 'DESC');
  $query->limit(20);

  $result = $query->execute();

  $max_pageviews = db_query("
    SELECT MAX(tgs.pageviews)
    FROM {node} n
    JOIN {tdc_ga_stats} tgs ON n.nid = tgs.nid AND tgs.type = 'total'
    WHERE n.uid = :uid AND n.type = 'event_listing'", array(':uid' => $user->uid))->fetchField();
  
    $header = array(
      array('data' => t('Event'), 'class'=>'event'),
      array('data' => t('Page Views'), 'class'=>'pageviews'),
      array('data' => t('Website'), 'class'=>'social'),
      array('data' => t('Facebook'), 'class'=>'social'),
      array('data' => t('Twitter'), 'class'=>'social'),
      array('data' => t('Blog'), 'class'=>'social'),
      array('data' => t('Yelp'), 'class'=>'social'),
      array('data' => t('Zagat'), 'class'=>'social'),
      array('data' => t('OpenTable'), 'class'=>'social'),
      array('data' => t('City Eats'), 'class'=>'social'),
    );

    // I shouldn't really make a module dependent on a theme
    $external_link_icon = theme('image', array('path'=>drupal_get_path('theme', 'aquincum') . '/images/icons/external_link.png'));

    foreach ( $result as $row ) {

      $rows[] = array(
        'data' => array(
          
          l($row->title, 'dashboard/events/' . ($row->is_past ? 'past/' : '') . '' . $row->nid, array('html'=>TRUE)) . ' ' . 
          l($external_link_icon, 'node/' . $row->nid, array('html'=>TRUE, 'attributes'=>array('target'=>'_blank'))),

          array('data' => '<div class="histogram" style="width: ' .
            (int)($row->pageviews/$max_pageviews*170) . 'px; "></div>' .
               $row->pageviews, 'class'=>'pageviews'),
          array('data' => $row->website, 'class'=>'social'),
          array('data' => $row->facebook, 'class'=>'social'),
          array('data' => $row->twitter, 'class'=>'social'),
          array('data' => $row->blog, 'class'=>'social'),
          array('data' => $row->yelp, 'class'=>'social'),
          array('data' => $row->zagat, 'class'=>'social'),
          array('data' => $row->opentable, 'class'=>'social'),
          array('data' => $row->cityeats, 'class'=>'social'),
        ),
      );
    }

    $summaries = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => FALSE,
        'empty' => t('There are no active events at this moment'),
        'attributes' => array(
          'class' => array('dTable', 'dataTable'),
          'id' => 'campaign_summary',
        ),
      )
    );
    
    $vars['pager'] = theme('pager', array('tags'=>array('First', 'Previous', '...', 'Next', 'Last')));

    global $pager_total, $pager_total_items, $pager_limits;
    $pg = pager_find_page();

    if ($pager_total_items[0] > 0) {
      $vars['pager_summary'] = 'Showing ' . ($pg * $pager_limits[0] + 1) . 
        ' to ' . min($pager_total_items[0], ($pg+1)*$pager_limits[0]) .
        ' of ' . $pager_total_items[0] . ' entries';
    }    
    
    $vars['summaries'] = $summaries;
}

/**
 * Preprocess for campaign-stats-overview-block.tpl.php
 */
function tdc_vendor_preprocess_campaign_stats_overview_block(&$vars) {
  
  global $user;

  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');


  $result = db_query("SELECT p.type, IFNULL(s.pageviews, 0) pageviews, IFNULL(s.purchases, 0) purchases, IFNULL(t.event_count, 0) events
FROM (
  SELECT 'year' AS type
  UNION
  SELECT 'month'
  UNION
  SELECT 'week'
) p
LEFT JOIN (
SELECT st.type, SUM(st.pageviews) pageviews, SUM(st.purchases) purchases
FROM tdc_ga_stats st
JOIN node n ON n.nid = st.nid AND n.type = 'event_listing'
JOIN field_data_field_promote_event pe ON 
  pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
WHERE n.uid = :uid AND n.status = 1 AND st.type <> 'total'
GROUP BY st.type
) s ON p.type = s.type
LEFT JOIN (
SELECT p.type, COUNT(*) event_count
FROM (
  SELECT 'year' AS type, UNIX_TIMESTAMP(CURDATE() - INTERVAL DAYOFYEAR(CURDATE()) DAY + INTERVAL 1 DAY) dt
  UNION
  SELECT 'month', UNIX_TIMESTAMP(CURDATE() - INTERVAL DAYOFMONTH(CURDATE()) DAY + INTERVAL 1 DAY)
  UNION
  SELECT 'week', UNIX_TIMESTAMP(CURDATE() - INTERVAL DAYOFWEEK(CURDATE()) DAY + INTERVAL 1 DAY)
) p
JOIN node n ON n.created > p.dt
JOIN field_data_field_promote_event pe ON 
  pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
WHERE n.uid = :uid AND n.status = 1 AND n.type = 'event_listing'
GROUP BY p.type
) t ON p.type = t.type
", array(':uid' => $user->uid));

  $type_to_id_map = array('year'=>'ytd-reports', 'month'=>'this-month-reports', 'week'=>'this-week-reports');

  foreach ($result as $row) {

    $vars['stats'][$type_to_id_map[$row->type]]['pageviews'] = $row->pageviews;
    $vars['stats'][$type_to_id_map[$row->type]]['purchases'] = $row->purchases;
    $vars['stats'][$type_to_id_map[$row->type]]['events'] = $row->events;
    $vars['stats'][$type_to_id_map[$row->type]]['type'] = $row->type;   
  }

  foreach ($type_to_id_map as $type => $id) {

    $chart_function = '_tdc_vendor_get_' . $type . '_chart';
        
    $vars['stats'][$type_to_id_map[$type]]['chart'] = $chart_function($type);
  }  
  $path = _flot_get_library_path();  
  drupal_add_js($path . '/jquery.flot.resize.js');

  $active_featured_event_count = db_query("SELECT COUNT(*)
FROM {node} n
JOIN {field_data_field_promote_event} pe ON 
  pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
WHERE n.type = 'event_listing' AND n.status = 1 AND n.uid = :uid", array(':uid' => $user->uid))->fetchField();

  $vars['active_featured_event_count'] = $active_featured_event_count;
}

function _tdc_vendor_get_year_chart($type) {

  global $user;

  $result = db_query("SELECT MONTH(STR_TO_DATE(date, '%Y%m%d')) month, SUM(IFNULL(pageviews, 0)) pageviews, SUM(IFNULL(purchases, 0)) purchases
FROM tdc_ga_chart_data
WHERE nid IN (
  SELECT n.nid 
  FROM node n
  JOIN field_data_field_promote_event pe ON 
    pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
  WHERE n.uid = :uid) AND YEAR(STR_TO_DATE(date, '%Y%m%d')) = YEAR(CURDATE())
GROUP BY MONTH(STR_TO_DATE(date, '%Y%m%d'))
ORDER BY MONTH(STR_TO_DATE(date, '%Y%m%d'))", array(':uid'=>$user->uid));

  foreach ($result as $row) {
    $purchases[] = array($row->month, $row->purchases);
    $pageviews[] = array($row->month, $row->pageviews);
  }

  $ticks = array();

  for ($i = 1; $i <= 12; $i++) {
    $ticks[] = array($i, date("M", mktime(0, 0, 0, $i, 10)));
  }

  $d1 = new flotData($pageviews);
  $d1->label = 'pageviews';
  $d2 = new flotData($purchases);
  $d2->label = 'purchase click-throughs';
  $chart_vars = array(
    'data' => array($d1, $d2),
    'element' => array(
      'id' => 'placeholder_' . $type,
      'class' => 'flot-chart',
      'style' => "100%;height:400px",
    ),
    'legend' => TRUE,  
    'options' => array(
        'grid' => array(
          'backgroundColor' => array('colors' => array('#fff', '#eee')),
        ),
        'series' => array(
          'lines' => array(
            'show' => true,
            ),
          'points' => array(
            'show' => true,
            ),
        ),
        'yaxis' => array(
          'tickDecimals' => 0,
          'min' => 0,
        ),
        'xaxis' => array(
          'ticks' => $ticks,
          'max' => 12,
          'min' => 1,
        ),
      ),   
  );

  $title = 'YTD - ' . date("Y");

  return array('graph'=>theme('flot_graph', $chart_vars), 'title'=>$title);
}

function _tdc_vendor_get_month_chart($type) {

  global $user;

  $result = db_query("SELECT DAY(STR_TO_DATE(date, '%Y%m%d')) day, SUM(IFNULL(pageviews, 0)) pageviews, SUM(IFNULL(purchases, 0)) purchases
FROM tdc_ga_chart_data
WHERE nid IN (
  SELECT n.nid 
  FROM node n
  JOIN field_data_field_promote_event pe ON 
    pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
  WHERE n.uid = :uid) AND YEAR(STR_TO_DATE(date, '%Y%m%d')) = YEAR(CURDATE()) AND 
  MONTH(STR_TO_DATE(date, '%Y%m%d')) = MONTH(CURDATE())
GROUP BY DAY(STR_TO_DATE(date, '%Y%m%d'))
ORDER BY DAY(STR_TO_DATE(date, '%Y%m%d'))", array(':uid'=>$user->uid));

  foreach ($result as $row) {
    $purchases[] = array($row->day, $row->purchases);
    $pageviews[] = array($row->day, $row->pageviews);
  }

  $d1 = new flotData($pageviews);
  $d1->label = 'pageviews';
  $d2 = new flotData($purchases);
  $d2->label = 'purchase click-throughs';
  $chart_vars = array(
    'data' => array($d1, $d2),
    'element' => array(
      'id' => 'placeholder_' . $type,
      'class' => 'flot-chart',
      'style' => "width:100%;height:400px",
    ),
    'legend' => TRUE,  
    'options' => array(
        'grid' => array(
          'backgroundColor' => array('colors' => array('#fff', '#eee')),
        ),
        'series' => array(
          'lines' => array(
            'show' => true,
            ),
          'points' => array(
            'show' => true,
            ),
        ),
        'yaxis' => array(
          'tickDecimals' => 0,
          'min' => 0,
        ),
        'xaxis' => array(
          'ticks' => 31,
          'max' => 31,
          'min' => 1,
          'tickDecimals' => 0,
          'tickSize' => 1,
        ),
      ),   
  );
  
  $title = 'Month - ' . date("F");

  return array('graph'=>theme('flot_graph', $chart_vars), 'title'=>$title);
}

function _tdc_vendor_get_week_chart($type) {
  global $user;

  $result = db_query("SELECT DAYOFWEEK(STR_TO_DATE(date, '%Y%m%d')) day, SUM(IFNULL(pageviews, 0)) pageviews, SUM(IFNULL(purchases, 0)) purchases
FROM tdc_ga_chart_data
WHERE nid IN (
  SELECT n.nid 
  FROM node n
  JOIN field_data_field_promote_event pe ON 
    pe.entity_id = n.nid AND pe.revision_id = n.vid AND pe.deleted = 0 AND pe.field_promote_event_value = 0
  WHERE n.uid = :uid) AND 
  STR_TO_DATE(date, '%Y%m%d') >= CURDATE() - INTERVAL DAYOFWEEK(CURDATE()) DAY + INTERVAL 1 DAY AND
  STR_TO_DATE(date, '%Y%m%d') < CURDATE() - INTERVAL DAYOFWEEK(CURDATE()) DAY + INTERVAL 8 DAY
GROUP BY DAYOFWEEK(STR_TO_DATE(date, '%Y%m%d'))
ORDER BY DAYOFWEEK(STR_TO_DATE(date, '%Y%m%d'))", array(':uid'=>$user->uid));


  foreach ($result as $row) {
    $purchases[] = array($row->day, $row->purchases);
    $pageviews[] = array($row->day, $row->pageviews);
  }

  $ticks = array();

  $timestamp = strtotime('last Sunday');
  $days = array();
  for ($i = 0; $i < 7; $i++) {
   $ticks[] = array($i+1, date('D n/d', $timestamp));
   $timestamp = strtotime('+1 day', $timestamp);
  }

  $d1 = new flotData($pageviews);
  $d1->label = 'pageviews';
  $d2 = new flotData($purchases);
  $d2->label = 'purchase click-throughs';
  $chart_vars = array(
    'data' => array($d1, $d2),
    'element' => array(
      'id' => 'placeholder_' . $type,
      'class' => 'flot-chart',
      'style' => "width:100%;height:400px",
    ),
    'legend' => TRUE,  
    'options' => array(
        'grid' => array(
          'backgroundColor' => array('colors' => array('#fff', '#eee')),
        ),
        'series' => array(
          'lines' => array(
            'show' => true,
            ),
          'points' => array(
            'show' => true,
            ),
        ),
        'yaxis' => array(
          'tickDecimals' => 0,
          'min' => 0,
        ),
        'xaxis' => array(
          'ticks' => $ticks,
          'max' => 7,
          'min' => 1,
        ),
      ),   
  );

  $title = 'Week - ' . date("m/d/Y", strtotime('last sunday')) . 
    ' through ' . date("m/d/Y", strtotime('next saturday'));

  return array('graph'=>theme('flot_graph', $chart_vars), 'title'=>$title);
}

/**
 * Preprocess for vendor-dashboard-header-block.tpl.php
 */
function tdc_vendor_preprocess_vendor_dashboard_header_block(&$vars) {
  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');

  $dashboard_menu = menu_tree('menu-dashboard-main');

  $dashboard_submenu = array();

  $path = current_path();

  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit') {
    $path = _tdc_vendor_get_vendor_event_path(arg(1));
  }

  $is_past_events_active = FALSE;
  $is_my_events_active = FALSE;

  if (strpos($path, 'dashboard/events/past') === 0)
    $is_past_events_active = TRUE;
  else if (strpos($path, 'dashboard/events') === 0)
    $is_my_events_active = TRUE;

  foreach ($dashboard_menu as $element) {
  	if ($element['#href'] == 'dashboard') {
              
  		$dashboard_submenu = $element['#below']; 

  		foreach ($dashboard_submenu as $key => $item) {

        if (($item['#href'] == 'dashboard/events/past' && $is_past_events_active) ||
          ($item['#href'] == 'dashboard/events' && $is_my_events_active)) {

          if (!in_array('active-trail', $item['#localized_options']['attributes']['class'])) {
            $dashboard_submenu[$key]['#localized_options']['attributes']['class'][] = 'active-trail';
            $dashboard_submenu[$key]['#attributes']['class'][] = 'active-trail';
          }
        }
  		}
  		break;
  	}
  }

  $dashboard_submenu['#theme_wrappers'] = array('menu_tree__menu_dashboard_main_events');

  $vars['add_new_event'] = l('Add New Event', 'node/add/event-listing', array('attributes' => 
            array('class' => array('buttonL', 'bGreen', 'add-new-event'))
            ));  
  $vars['dashboard_submenu'] = $dashboard_submenu;  
  $vars['breadcrumbs'] = theme('breadcrumb', array('breadcrumb' => _tdc_vendor_get_breadcrumb()));
  $vars['title'] = drupal_get_title();
}

/**
 * Preprocess for vendor-event-list-block.tpl.php
 */
function tdc_vendor_preprocess_event_list_block(&$vars) {
  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');  

  global $user;
  $links = array();

  $is_edit_mode = (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit');
  $path = $is_edit_mode ? _tdc_vendor_get_vendor_event_path(arg(1)) : current_path();

  if (arg(0, $path) == 'dashboard' && arg(1, $path) == 'events') {

  	$is_past = arg(2, $path) == 'past';
  	$summary_path = 'dashboard/events' . ($is_past ? '/past' : '');

  	$link = array(
	  	'#theme' => 'link',
	  	'#text' => '<span class="icos-graph"></span>Summary',
	  	'#path' => $summary_path,
	  	'#options' => array(
	  		'attributes' => array('class' => array()),
	  		'html' => TRUE,
  		),
  	);

  	$links[] = $link;

  }

  $result = db_query("SELECT n.nid, n.title
    FROM {node} n
    JOIN {field_data_field_date_time} dt ON dt.entity_id = n.nid AND dt.revision_id = n.vid AND dt.field_date_time_value " . ($is_past ? '<' : '>') . " NOW()
    WHERE n.uid = :uid AND n.type = 'event_listing'
    ORDER BY dt.field_date_time_value DESC", array(':uid' => $user->uid));

  foreach ($result as $row) {
  	
  	$event_path = 'dashboard/events/' . ($is_past ? 'past/' : '') . $row->nid;

	  $link = array(
	  	'#theme' => 'link',
	  	'#text' => '<span class="icos-mcalendar"></span>' . $row->title,
	  	'#path' => $event_path,
	  	'#options' => array(
	  		'attributes' => array('class' => array()),
	  		'html' => TRUE,
	  	),
	  );

	  $links[] = $link;	
  }

  foreach ($links as $key => $link) {
  	if ($path == $link['#path']) {
  		$links[$key]['#options']['attributes']['class'][] = 'this';
  		$links[$key]['#options']['attributes']['class'][] = 'active';
  	}
  }

  $last_key = array_pop(array_keys($links));
  
  if ($last_key) {
  	$links[$last_key]['#options']['attributes']['class'][] = 'noBorderB';
  }
  
  $vars['event_list'] = theme('event_list', array('links' => $links));
}

/**
 * Preprocess for vendor-picture-block.tpl.php
 */
function tdc_vendor_preprocess_vendor_picture_block(&$vars) {
  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');

  global $base_url;
  global $user;
  $uid = $user->uid;
  $user_obj = user_load($uid);

  if (isset($user_obj->picture)) {
    $img = image_style_url('vendor_profile', $user_obj->picture->uri);
  } else {
    $img = $base_url . '/sites/all/themes/custom/aquincum/images/icons/default_profile_image.png';
  }

  $vars['imgurl'] = $img;
  $vars['userurl'] = $base_url . '/user/' . $user->uid;
  $vars['username'] = $user->name;
}

function _tdc_vendor_get_breadcrumb() { 

  $breadcrumb = array();

  $is_edit_mode = (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit');

  $path = $is_edit_mode ? _tdc_vendor_get_vendor_event_path(arg(1)) : current_path();
  $event_title = drupal_get_title();

  if ($is_edit_mode) {
    $node = node_load(arg(1));
    $event_title = $node->title;
  }

  if (arg(0, $path) == 'dashboard' || current_path() == 'node/add/event-listing') {

    $breadcrumb[] = array('title' => 'Dashboard', 'href' => 'dashboard');

    if (arg(1, $path) == 'events') {
      if (arg(2, $path) == 'past') {
        $breadcrumb[] = array('title' => 'Past Events', 'href' => 'dashboard/events/past');
        if (is_numeric(arg(3, $path))) {
          $breadcrumb[] = array('title' => $event_title, 'href' => 'dashboard/events/past/' . arg(3, $path));
        }
      }
      else {
        $breadcrumb[] = array('title' => 'My Events', 'href' => 'dashboard/events');
        if (is_numeric(arg(2, $path))) {
          $breadcrumb[] = array('title' => $event_title, 'href' => 'dashboard/events/' . arg(2, $path));
        }
      }
    }
    else if (current_path() == 'node/add/event-listing') {
      $breadcrumb[] = array('title' => 'Add New Event', 'href' => 'node/add/event-listing');
    }
    else if ( strstr(current_path(), 'dashboard/add-new-event/') ) {
      $breadcrumb[] = array('title' => 'Add New Event', 'href' => 'dashboard/add-new-event/' . arg(2));
    }
    
    if ($is_edit_mode) {                  
      $breadcrumb[] = array('title' => 'Edit', 'href' => current_path());
    }
  }  
	
	foreach ($breadcrumb as $key=>$item) {
		$breadcrumb[$key] = '<li' . ($item['href'] == current_path() ? ' class="current"' : '') . '>' .
     l($item['title'], $item['href'], array('html' => TRUE)) . '</li>';
	}	

	return $breadcrumb;	
}

/**
 * Preprocess for event-stats-block.tpl.php
 */
function tdc_vendor_preprocess_event_stats_block(&$vars) {
  global $user;

  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();
  $vars['classes_array'] = array('');

  $title = "";
  $event_submitted = "";
  $event_url = "";
  $edit_event_url = "";
  $use_as_template_url = "";
  $event_details = "";
  $event_stats = "";
  $event_status = 0;
  $tickets_available_form = "";

  if ( arg(1) == 'events' && arg(2) == 'past' && is_numeric(arg(3)) ) {
    $node = node_load(arg(3));
  }
  elseif ( arg(1) == 'events' && is_numeric(arg(2)) ) {
    $node = node_load(arg(2));
  }

  $featured_event = FALSE;

  if (isset($node->field_promote_event[LANGUAGE_NONE][0]['value']) && $node->field_promote_event[LANGUAGE_NONE][0]['value'] == 0) {
    $featured_event = TRUE;
  }

  if (isset($node->type) && node_access('update', $node, $user)) {

    $has_access = TRUE;

    $title = $node->title;
    if ( $node->status == 1 && $node->created != $node->changed ) {
      $event_submitted = t('Submitted !date | Published !pub-date',
        array(
          '!date' => format_date($node->created, 'short'),
          '!pub-date' => format_date($node->changed, 'short'),
        )
      );
    }
    else {
      $event_submitted = t('Submitted !date', array('!date' => format_date($node->created, 'short')));
    }

    $event_url = url('node/' . $node->nid);
    $edit_event_url = url('node/' . $node->nid . '/edit');
    $use_as_template_url = url('dashboard/add-new-event/' . $node->nid);

    $event_status = $node->status;

    // Page visits and Purchase ticket click-thoughs.
    $stats_query = db_query("SELECT * FROM {tdc_ga_stats} s 
      JOIN {node} n ON n.nid = s.nid 
      WHERE s.type = 'total' AND n.uid = :uid AND n.nid = :nid", array(':uid' => $user->uid, ':nid' => $node->nid))->fetchObject();

    $sites = array(
      'facebook' => t('Facebook'),
      'twitter' => t('Twitter'),
      'blog' => t('Blog'),
      'yelp' => t('Yelp'),
      'zagat' => t('Zagat'),
      'opentable' => t('OpenTable'),
      'cityeats' => t('City Eats'),
      'website' => t('Your Website'),
    );

    $max = max($stats_query->pageviews, $stats_query->purchases);
    $min = min($stats_query->pageviews, $stats_query->purchases);
    $percentage = ((($max - $min) / $max) * 100);

    if ( $stats_query->pageviews == $max) {
      $pageviews_width = 100;
      $purchases_width = 100 - $percentage;
    }
    else {
      $pageviews_width = 100 - $percentage;
      $purchases_width = 100;
    }

    $vars['stats']['bars']['pageviews'] = array(
      'count' => isset($stats_query->pageviews) ? $stats_query->pageviews : 0,
      'width' => isset($stats_query->pageviews) ? $pageviews_width : 0,
    );

    $vars['stats']['bars']['purchases'] = array(
      'count' => isset($stats_query->purchases) ? $stats_query->purchases : 0,
      'width' => isset($stats_query->purchases) ? $purchases_width : 0,
    );

    foreach ( $sites as $key => $value ) {
      if ( isset($stats_query->{$key}) ) {
        $vars['stats']['sites'][] = array(
          'name' => $value,
          'clicks' => !empty($stats_query->{$key}) ? $stats_query->{$key} : 0,
        );
      }
    }

    // Top Domains
    $top_domains_query = db_query("SELECT * FROM {tdc_ga_sources} s
      WHERE s.type = 'referral' AND s.nid = :nid ORDER BY s.visits DESC LIMIT 6", array(':nid' => $node->nid));
    $header = array(
      array('data' => t(''),),
    );
    foreach ( $top_domains_query as $row ) {

      $rows[] = array(
        'data' => array(
          array('data' => $row->source),
        ),
      );
    }

    $top_domains_table = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => FALSE,
        'empty' => t('No Google Analytics data to show. The data is imported every hour.'),
        'attributes' => array(
          'class' => array('dTable', 'dataTable'),
          'id' => 'top_domains',
        ),
      )
    );

    $rows = array();

    // Top Keyword Searches
    $top_keyword_searches_query = db_query("SELECT * FROM {tdc_ga_sources} s
      WHERE s.type = 'keyword' AND s.nid = :nid ORDER BY s.visits DESC LIMIT 6", array(':nid' => $node->nid));
    $header = array(
      array('data' => t(''),),
    );
    foreach ( $top_keyword_searches_query as $row ) {

      $rows[] = array(
        'data' => array(
          array('data' => $row->source),
        ),
      );
    }

    $top_keyword_searches_table = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => FALSE,
        'empty' => t('No Google Analytics data to show. The data is imported every hour.'),
        'attributes' => array(
          'class' => array('dTable', 'dataTable'),
          'id' => 'top_keyword_searches',
        ),
      )
    );

    // Only if paid event
    if ( $node->field_paid_event[LANGUAGE_NONE][0]['value'] == 0 ) {
      $tickets_form = drupal_get_form('tdc_vendor_tickets_available_form', $node);
      $tdc_vendor_tickets_available_form = drupal_render($tickets_form);
    }

    $vars['has_access'] = $has_access;
    $vars['event_node'] = $node;
    $node_event = node_view($node, 'events_stats_info');
    $event_details = drupal_render($node_event);

  }

  $vars['top_domains_table'] = isset($top_domains_table) ? $top_domains_table : '';
  $vars['top_keyword_searches_table'] = isset($top_keyword_searches_table) ? $top_keyword_searches_table : '';
  $vars['tickets_available_form'] = $tdc_vendor_tickets_available_form;
  $vars['use_as_template_url'] = $use_as_template_url;
  $vars['event_url'] = $event_url;
  $vars['edit_event_url'] = $edit_event_url;
  $vars['event_status'] = $event_status;
  $vars['event_title'] = $title;
  $vars['event_details'] = $event_details;
  $vars['event_submitted'] = $event_submitted;
  $vars['event_stats'] = $event_stats;
  $vars['featured_event'] = $featured_event;

}

