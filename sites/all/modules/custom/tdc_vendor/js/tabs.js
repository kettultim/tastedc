/**
 * @file
 * JS Helper
 */

(function ($) {

  Drupal.behaviors.tdcVendorTabs = {
    attach: function(context) {

      $('ul#report-tabs').each(function(){
          // For each set of tabs, we want to keep track of
          // which tab is active and it's associated content
          var $active, $content, $links = $(this).find('a');

          var hash = location.hash || '#ytd-reports';

          // If the location.hash matches one of the links, use that as the active tab.
          // If no match is found, use the first link as the initial active tab.
          $active = $($links.filter('[href="'+hash+'"]')[0] || $links[0]);
          $active.parent('li').addClass('activeTab');
          $content = $($active.attr('href')).show();

          // Hide the remaining content
          $links.not($active).each(function () {
              $($(this).attr('href')).hide();
          });

          // Bind the click event handler
          $(this).find('a').click(function(e){
              // Make the old tab inactive.
              $active.parent('li').removeClass('activeTab');
              $content.hide();

              // Update the variables with the new link and content
              $active = $(this);
              $content = $($(this).attr('href'));

              // Make the tab active.
              $active.parent('li').addClass('activeTab');
              $content.show();

              // Prevent the anchor's default click action
              e.preventDefault();
          });
      });

    }
  };
}(jQuery));