<?php global $base_url; ?>

<div class="tdc-popup-close-icon">
<a href="javascript:void(0);" id="tdc-popup-close-link" alt="x">
  <img src="<?php echo $base_url ?>/sites/all/modules/custom/tdc_popup/css/images/close.png">
</a>
</div>

<div class="tdc-popup-logo"><img src="<?php echo $base_url ?>/sites/default/files/logo.jpg"></div>
 
<div class="tdc-popup-form">
<div class="intro-text">
  <p>Subscribe to the TasteDC Food and Drink Event Calendar</p>
</div>

<?php echo drupal_render_children($form) ?>
</div> 