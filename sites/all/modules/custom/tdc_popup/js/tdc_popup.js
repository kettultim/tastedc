(function ($) {
  
  // This script is only called if user is anonymous
  Drupal.behaviors.tdc_popup = {
    attach: function(context) {
      
      // Cookie write helper
      function tdcWriteCookie(name, value, duration) {
        if (duration) {
          var date = new Date();
          date.setTime(date.getTime() + (duration * 1000));
          var expires = "; expires="+date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
      }
      
      // Cookie read helper
      function tdcReadCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') c = c.substring(1, c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
      }      
    
      // Popup close event handler
      function tdcDialogClose(event, ui) {
        // Set cookie for 1 hour not to show this popup
        tdcWriteCookie('tdc_popup_close', 1, 60*60);
      }
      
      // Get popup state
      var show_popup = true;
      var tdc_popup_submitted = tdcReadCookie('tdc_popup_submitted');
      var tdc_popup_close = tdcReadCookie('tdc_popup_close');
      if(tdc_popup_submitted == 1 || tdc_popup_close == 1) show_popup = false;
      
      // Show popup if needed
      if(show_popup) {
        $('#edit-submit--2').removeClass('form-submit');
        $('#tdc-popup-close-link').click(function(){$('.block-tdc-popup').dialog( "close" );});
        
        $('.block-tdc-popup').dialog({
          close: tdcDialogClose,
          width: 380,
          height: 390,
          resizable: false,
          modal: true,
          dialogClass: 'tdc-popup-ui-dialog',
          open: function(event, ui) {
            $('#edit-email--2').focus();
          }
        });        
      }
            
    }
  }

}(jQuery));