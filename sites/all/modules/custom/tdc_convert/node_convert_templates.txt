﻿# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.25)
# Database: tastedc_dev
# Generation Time: 2013-01-26 23:01:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table node_convert_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `node_convert_templates`;

CREATE TABLE `node_convert_templates` (
  `nctid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `source_type` tinytext,
  `destination_type` tinytext NOT NULL,
  `data` mediumtext,
  PRIMARY KEY (`nctid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `node_convert_templates` WRITE;
/*!40000 ALTER TABLE `node_convert_templates` DISABLE KEYS */;

INSERT INTO `node_convert_templates` (`nctid`, `name`, `source_type`, `destination_type`, `data`)
VALUES
	(2,'Free events -> Event','free_event_listing','event_listing','a:3:{s:6:\"fields\";a:2:{s:6:\"source\";a:10:{i:0;s:11:\"field_price\";i:1;s:14:\"field_category\";i:2;s:11:\"field_image\";i:3;s:16:\"field_event_type\";i:4;s:15:\"field_date_time\";i:5;s:19:\"field_sale_end_date\";i:6;s:14:\"field_location\";i:7;s:11:\"field_phone\";i:8;s:22:\"field_free_description\";i:9;s:22:\"field_location_website\";}s:11:\"destination\";a:10:{i:0;s:11:\"field_price\";i:1;s:14:\"field_category\";i:2;s:11:\"field_image\";i:3;s:16:\"field_event_type\";i:4;s:15:\"field_date_time\";i:5;s:19:\"field_sale_end_date\";i:6;s:14:\"field_location\";i:7;s:11:\"field_phone\";i:8;s:23:\"field_short_description\";i:9;s:22:\"field_location_website\";}}s:12:\"hook_options\";N;s:9:\"no_fields\";b:0;}'),
	(3,'Paid events -> Event','paid_event_listing','event_listing','a:3:{s:6:\"fields\";a:2:{s:6:\"source\";a:13:{i:0;s:4:\"body\";i:1;s:16:\"field_event_type\";i:2;s:11:\"field_price\";i:3;s:14:\"field_category\";i:4;s:11:\"field_image\";i:5;s:14:\"field_subtitle\";i:6;s:15:\"field_date_time\";i:7;s:14:\"field_location\";i:8;s:13:\"field_image_2\";i:9;s:19:\"field_sale_end_date\";i:10;s:21:\"field_buy_ticket_link\";i:11;s:12:\"field_phone2\";i:12;s:22:\"field_location_website\";}s:11:\"destination\";a:13:{i:0;s:4:\"body\";i:1;s:16:\"field_event_type\";i:2;s:11:\"field_price\";i:3;s:14:\"field_category\";i:4;s:11:\"field_image\";i:5;s:14:\"field_subtitle\";i:6;s:15:\"field_date_time\";i:7;s:14:\"field_location\";i:8;s:13:\"field_image_2\";i:9;s:19:\"field_sale_end_date\";i:10;s:21:\"field_buy_ticket_link\";i:11;s:11:\"field_phone\";i:12;s:22:\"field_location_website\";}}s:12:\"hook_options\";N;s:9:\"no_fields\";b:0;}');

/*!40000 ALTER TABLE `node_convert_templates` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
