<?php
/**
 * @file
 * purchase_a_listing_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function purchase_a_listing_content_type_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Purchase a Listing'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
