<?php
/**
 * @file
 * event_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function event_content_type_node_info() {
  $items = array(
    'event_listing' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Use this content type to create events.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
